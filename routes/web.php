<?php

use App\Http\Controllers\AssessmentController;
use App\Http\Controllers\AssessmentResultController;
use App\Http\Controllers\CriteriaAssessmentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MappingAlternativeController;
use App\Http\Controllers\SAWController;
use App\Http\Controllers\UserAssessmentController;
use App\Http\Controllers\UserAssessmentResponseController;
use App\Http\Controllers\WeightCriteriaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/bobot', [SAWController::class, 'index'])->name('bobot');

// kriteria penilaian
Route::get('kriteria-penilaian', [AssessmentController::class, 'index'])->name('kriteria-penilaian');
Route::get('kriteria-penilaian/create', [AssessmentController::class, 'create'])->name('kriteria-penilaian-create');
Route::post('kriteria-penilaian/create', [AssessmentController::class, 'store'])->name('kriteria-penilaian-store');
Route::get('kriteria-penilaian/{id}/edit', [AssessmentController::class, 'show'])->name('kriteria-penilaian-show');
Route::put('kriteria-penilaian/{id}/edit', [AssessmentController::class, 'update'])->name('kriteria-penilaian-update');
Route::get('kriteria-penilaian/{id}/review', [CriteriaAssessmentController::class, 'show'])->name('kriteria-penilaian-review-show');
Route::put('kriteria-penilaian/{id}/review', [CriteriaAssessmentController::class, 'update'])->name('kriteria-penilaian-review-update');
Route::get('kriteria-penilaian/{id}/preview', [CriteriaAssessmentController::class, 'edit'])->name('kriteria-penilaian-preview-show');
Route::get('kriteria-penilaian/{id}/publish', [AssessmentController::class, 'publish'])->name('kriteria-penilaian-publish');
Route::get('kriteria-penilaian/{id}/unpublish', [AssessmentController::class, 'unpublish'])->name('kriteria-penilaian-unpublish');

Route::get('bobot-kriteria', [WeightCriteriaController::class, 'index'])->name('bobot-kriteria-index');
Route::get('bobot-kriteria/{id}/generate', [WeightCriteriaController::class, 'getWeight'])->name('bobot-kriteria-generate');

// mapping alternative
Route::get('mapping-alternatif', [MappingAlternativeController::class, 'index'])->name('mapping-alternative-index');
Route::post('mapping-alternatif', [MappingAlternativeController::class, 'store'])->name('mapping-alternative-store');
Route::delete('mapping-alternatif/{id}', [MappingAlternativeController::class, 'destroy'])->name('mapping-alternative-destroy');

// penilaian alternatif
Route::get('/penilaian-alternatif', [UserAssessmentController::class, 'index'])->name('penilaian-alternatif-index');
Route::get('/penilaian-alternatif/{assessmentId}/{userId}', [UserAssessmentController::class, 'show'])->name('penilaian-alternatif-show');
Route::put('/penilaian-alternatif/{assessmentId}/{userId}', [UserAssessmentController::class, 'update'])->name('penilaian-alternatif-update');
Route::get('/penilaian-response', [UserAssessmentResponseController::class, 'index'])->name('response-penilaian-index');

// penilaian
Route::get('/rank-penilaian', [AssessmentResultController::class, 'index'])->name('penilaian-result-index');

