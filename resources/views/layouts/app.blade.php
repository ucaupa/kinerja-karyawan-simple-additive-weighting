<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Penilaian Kinerja Karyawan') }}</title>

    <!-- base:css -->
    <link rel="stylesheet" href="{{ asset('vendors/typicons.font/font/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/data-tables/datatables.min.css') }}">
    <!-- plugin css for this page -->
    @yield('css-plugins')
    <style>
        .cursor-pointer {
            cursor: pointer !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 8px;
        }

        .gap-1 {
            gap: 2px;
        }

        .gap-2 {
            gap: 4px;
        }

        .gap-3 {
            gap: 6px;
        }

        .gap-4 {
            gap: 8px;
        }

        .cursor-move {
            cursor: move;
        }

        .badge-warning {
            color: white !important;
        }
    </style>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>

    @yield('styles')
</head>
<body class="sidebar-light">
<div id="app">
    <div class="container-scroller">
        @include('layouts.header')

        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            {{--@include('layouts.sidebar-setting')--}}
            @include('layouts.sidebar')


            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('content')
                </div>

                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-center text-sm-left d-block d-sm-inline-block">
                            Copyright ©
                            <a href="#">Maulana Yusup | 14177015</a>
                            - 2023
                        </span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
                            Aplikasi <strong>Penilaian Kinerja Karyawan</strong> Metode <strong>Simple Additive Weighting</strong>
                        </span>
                    </div>
                </footer>
            </div>
        </div>
    </div>

    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Plugin js for this page-->
    <script src="{{ asset('vendors/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <!-- plugin js for this page -->
    <script src="{{ asset('vendors/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendors/data-tables/datatables.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.textarea-height-auto').each(function (key, value) {
                value.style.overflow = 'hidden';
                value.style.height = 0;
                value.style.height = value.scrollHeight + 'px';
            });
        });

        $(document).on('keydown keyup keypress', '.textarea-height-auto', function () {
            this.style.overflow = 'hidden';
            this.style.height = 0;
            this.style.height = this.scrollHeight + 'px';
        });

        @if (Session::has('success'))
        Swal.fire({
            title: '{!! Session::get('success') !!}',
            text: '{!! Session::has('messages') ? Session::get('messages') : '' !!}',
            icon: 'success'
        });
        @endif
        @if (Session::has('error'))
        Swal.fire({
            title: '{!! Session::get('error') !!}',
            text: '{!! Session::has('messages') ? Session::get('messages') : '' !!}',
            icon: 'error'
        });
        @endif
        @if (Session::has('info'))
        Swal.fire({
            title: '{!! Session::get('info') !!}',
            text: '{!! Session::has('messages') ? Session::get('messages') : '' !!}',
            icon: 'info'
        });
        @endif
    </script>
    @stack('scripts')
</div>
</body>
</html>
