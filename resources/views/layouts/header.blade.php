<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-info">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="#">
            <img src="{{ asset('images/logo.svg') }}" alt="logo"/>
        </a>
        <a class="navbar-brand brand-logo-mini" href="#">
            <img src="{{ asset('images/logo-mini.svg') }}" alt="logo"/>
        </a>
        <button
            class="navbar-toggler navbar-toggler align-self-center d-none d-lg-flex"
            type="button"
            data-toggle="minimize">
            <span class="typcn typcn-th-menu"></span>
        </button>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item dropdown d-flex mr-2">
                <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center"
                   id="notificationDropdown" href="#" data-toggle="dropdown">
                    <i class="typcn typcn-bell mr-0"></i>
                    @if(isset($totalAssessmentNotification) && $totalAssessmentNotification > 0)
                        <span class="count bg-danger">{{ $totalAssessmentNotification }}</span>
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
                     aria-labelledby="notificationDropdown">
                    @if($totalAssessmentNotification == 0)
                        <p class="mb-0 font-weight-normal float-left dropdown-header">Belum ada pemberitahuan</p>
                    @endif
                    @if(isset($teamAssessmentList) && count($teamAssessmentList) > 0)
                        <p class="mb-0 font-weight-normal float-left dropdown-header">Beri Penilaian Kriteria</p>
                        @foreach($teamAssessmentList as $assessment)
                            <a class="dropdown-item preview-item cursor-pointer"
                               href="{{ route('kriteria-penilaian-review-show', ['id' => $assessment->id]) }}">
                                <div class="preview-thumbnail">
                                    <div class="preview-icon bg-behance" style="width: 25px; height: 25px;">
                                        <i class="typcn typcn-puzzle mx-0"></i>
                                    </div>
                                </div>
                                <div class="preview-item-content">
                                    <h6 class="preview-subject font-weight-bold">{{ $assessment->title }}</h6>
                                    <p class="font-weight-light text-small mb-0">
                                        {{ $assessment->created_at->diffForHumans() }}
                                    </p>
                                </div>
                            </a>
                        @endforeach
                    @endif
                    @if(isset($userAssessmentList) && count($userAssessmentList) > 0)
                        <p class="mb-0 font-weight-normal float-left dropdown-header">Beri Penilaian Alternatif</p>
                        @foreach($userAssessmentList as $assessments)
                            <p class="mb-0 font-weight-normal text-small float-left dropdown-header py-0 font-italic">
                                {{ $assessments->title }}
                            </p>
                            @foreach($assessments->userAssessments as $userAssessment)
                                <a class="dropdown-item preview-item cursor-pointer"
                                   href="{{ route('penilaian-alternatif-show', [$assessments->id, $userAssessment->user_id]) }}">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-linkedin" style="width: 25px; height: 25px;">
                                            <i class="typcn typcn-user mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-bold">
                                            {{ $userAssessment->user->name }}
                                        </h6>
                                        <p class="font-weight-light text-small mb-0">
                                            {{ $assessments->created_at->diffForHumans() }}
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        @endforeach
                    @endif
                </div>
            </li>
            <li class="nav-item ml-3">
                <a class="nav-link pl-0 pr-0" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <span class="nav-profile-name">{{ __('Logout') }}</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                data-toggle="offcanvas">
            <span class="typcn typcn-th-menu"></span>
        </button>
    </div>
</nav>
