<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <div class="d-flex sidebar-profile">
                <div class="sidebar-profile-image">
                    <img src="{{ asset('images/samples/300x300/1.jpg') }}" alt="image">
                    <span class="sidebar-status-indicator"></span>
                </div>
                <div class="sidebar-profile-name">
                    <p class="sidebar-name">
                        {{ auth()->user()->name }}
                    </p>
                    <p class="sidebar-designation">
                        {{ auth()->user()->email }}
                    </p>
                </div>
            </div>
        </li>
        <li class="nav-item  {{ request()->routeIs('home') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="typcn typcn-device-desktop menu-icon"></i>
                <span class="menu-title">Home</span>
            </a>
        </li>
        <li class="nav-item {{ request()->routeIs('penilaian-alternatif*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('penilaian-alternatif-index') }}">
                <i class="typcn typcn-document-text menu-icon"></i>
                <span class="menu-title">Penilaian Alternatif</span>
            </a>
        </li>
        @if(auth()->user()->is_role_management)
            <li class="nav-item">
                <p class="sidebar-menu-title">Master</p>
            </li>
            <li class="nav-item {{ request()->routeIs('kriteria-penilaian*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kriteria-penilaian') }}">
                    <i class="typcn typcn-puzzle-outline menu-icon"></i>
                    <span class="menu-title">Kriteria Penilaian</span>
                </a>
            </li>
            <li class="nav-item {{ request()->routeIs('bobot-kriteria-index*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('bobot-kriteria-index') }}">
                    <i class="typcn typcn-puzzle menu-icon"></i>
                    <span class="menu-title">Bobot Kriteria</span>
                </a>
            </li>
            <li class="nav-item {{ request()->routeIs('mapping-alternative-index*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('mapping-alternative-index') }}">
                    <i class="typcn typcn-group menu-icon"></i>
                    <span class="menu-title">Mapping Alternatif</span>
                </a>
            </li>
            <li class="nav-item {{ request()->routeIs('response-penilaian-index*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('response-penilaian-index') }}">
                    <i class="typcn typcn-database menu-icon"></i>
                    <span class="menu-title">Semua Penilaian</span>
                </a>
            </li>
            <li class="nav-item {{ request()->routeIs('penilaian-result-index*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('penilaian-result-index') }}">
                    <i class="typcn typcn-chart-line menu-icon"></i>
                    <span class="menu-title">Peringkat Alternatif</span>
                </a>
            </li>
        @endif
    </ul>
</nav>
