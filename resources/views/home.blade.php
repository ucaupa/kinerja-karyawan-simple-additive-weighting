@extends('layouts.app')

@section('title', 'Home')

@section('css-plugins')
@endsection

@section('styles')
@endsection

@section('content')
    <div class="row mt-3">
        @if(isset($userAssessmentList) && count($userAssessmentList) > 0)
            <div class="col-lg-5 d-flex grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-wrap justify-content-between">
                            <h4 class="card-title mb-0">Beri Penilaian</h4>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        @foreach($userAssessmentList as $assessment)
                                            <div class="d-flex justify-content-between mt-4 mb-3">
                                                <div
                                                    class="font-weight-medium font-italic">{{ $assessment->title }}</div>
                                            </div>
                                            @foreach($assessment->userAssessments as $userAssessment)
                                                <div class="d-flex justify-content-between mb-2">
                                                    <a href="{{ route('penilaian-alternatif-show', [$assessment->id, $userAssessment->user_id]) }}"
                                                       class="text-secondary font-weight-medium">
                                                        {{ $userAssessment->user->name }}
                                                    </a>
                                                    <div class="small">{{ $assessment->created_at->diffForHumans() }}</div>
                                                </div>
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


@push('scripts')
@endpush
