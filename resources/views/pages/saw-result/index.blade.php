@extends('layouts.app')

@section('title', 'Mapping Alternatif')

@section('css-plugins')
@endsection

@section('styles')
@endsection

@section('content')
    <div class="row mt-3">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3">
                        <h4 class="card-title">Peringkat Alternatif / Karyawan</h4>
                    </div>

                    <div class="d-flex flex-column flex-sm-row justify-content-start align-items-end mb-3">
                        <div class="form-group col-md-8">
                            <label for="filter-assessment">Judul Penilaian</label>
                            <select class="form-control select2 select2-filter" id="filter-assessment"
                                    style="width: 100%;">
                                <option></option>
                                @foreach($assessments as $assessment)
                                    @php
                                        $selected = $assessment->id == request()->get('id') ? 'selected' : '';
                                    @endphp
                                    <option
                                        value="{{ $assessment->id }}" {{ $selected }}>{{ $assessment->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <button class="btn btn-info btn-block check">Cek Peringkat</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!empty($rank))
        <div class="row">
            <div class="col-lg-5 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2" width="20px">Kode</th>
                                    <th class="p-2">Nama</th>
                                    <th class="p-2" width="110px">Nilai</th>
                                    <th class="p-2" width="100px">Peringkat</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rank as $key => $item)
                                    <tr>
                                        <td class="p-2 text-center">{{ $item['code'] }}</td>
                                        <td class="p-2 text-left">{{ $item['alternative'] }}</td>
                                        <td class="p-2 text-right">{{ number_format($item['value'] * 100, 4) }}</td>
                                        <td class="p-2 text-center font-weight-bold">{{ $key + 1 }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Parameter Penilaian</h4>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2">Kode</th>
                                    <th class="p-2">Kriteria</th>
                                    <th class="p-2">Jenis</th>
                                    <th class="p-2">Bobot</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($weight as $key => $item)
                                    <tr>
                                        <td class="p-2">C{{ $key + 1 }}</td>
                                        <td class="p-2 font-weight-bold">{{ $item['criteria'] }}</td>
                                        <td class="p-2">{{ $item['type'] }}</td>
                                        <td class="p-2">{{ $item['value'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex justify-content-between mt-4">
                            <h4 class="text-small">Data Alternatif</h4>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2">Alternatif</th>
                                    @foreach($dataAlternatif[0]['value'] as $key => $value)
                                        <th class="p-2">C{{ $key + 1 }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dataAlternatif as $item)
                                    <tr>
                                        <td class="p-2 font-weight-bold">{{ $item['code'] }}</td>
                                        @foreach($item['value'] as $key => $value)
                                            <td class="p-2">{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex justify-content-between mt-4">
                            <h4 class="text-small">Normalisasi Data</h4>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2">Alternatif</th>
                                    @foreach($normalizedData[0]['value'] as $key => $value)
                                        <th class="p-2">C{{ $key + 1 }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($normalizedData as $item)
                                    <tr>
                                        <td class="p-2 font-weight-bold">{{ $item['code'] }}</td>
                                        @foreach($item['value'] as $key => $value)
                                            <td class="p-2">{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif(!empty(request()->get('id')))
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body text-center text-gray">
                        <h4 class="m-0">Belum ada penilaian</h4>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            let assessment = $("#filter-assessment").select2({
                placeholder: 'Pilih Penilaian'
            });

            $('.check').click(function () {
                window.location = '{{ route('penilaian-result-index') }}?id=' + $(assessment).val();
            });
        });
    </script>
@endpush
