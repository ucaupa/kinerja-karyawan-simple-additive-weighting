@extends('layouts.app')

@section('title', 'Kriteria Penilaian')

@section('css-plugins')
@endsection

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3">
                        <h4 class="card-title">Bobot Kriteria Penilaian</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-sm">
                            <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Bobot</th>
                                <th style="width: 50px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td colspan="4" class="text-center">Belum ada data</td>
                                </tr>
                            @endif
                            @foreach($data as $item)
                                <tr>
                                    <td class="valign-top" style="width: 30%;">
                                        <b>{{ $item->title }}</b>
                                    </td>
                                    <td class="valign-center" style="max-width: 50%;">
                                        @if(!empty($item->weight))
                                            <ul>
                                                @foreach(json_decode($item->weight, true) as $key => $weight)
                                                    <li>
                                                        <strong>{{ $item->items[$key]->title }}</strong>: {{ $weight }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="valign-top" style="width: 10%;">
                                        <button type="button"
                                                class="btn btn-info btn-sm mt-2 btn-publish"
                                                style="font-size: 12px"
                                                data-id="{{ $item->id }}"
                                                data-weight="{{ $item->weight }}">
                                            Edit
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @if(\Illuminate\Support\Facades\Session::has('dataObjective'))
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title"><u>Proses Penentuan Bobot</u></h4>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Data Penilaian oleh Tim pada <i>{{ \Illuminate\Support\Facades\Session::get('title') }}</i></h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2" width="20px">Kode</th>
                                    <th class="p-2">Nama</th>
                                    @foreach(Session::get('dataObjective')[0]['value'] as $key => $item)
                                        <th class="p-2">C{{ $key + 1 }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Session::get('dataObjective') as $key => $item)
                                    <tr>
                                        <td class="p-2 text-center">{{ $item['code'] }}</td>
                                        <td class="p-2 text-left">{{ $item['alternative'] }}</td>
                                        @foreach($item['value'] as $value)
                                            <td class="p-2 text-right">{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex justify-content-between mt-4">
                            <h4 class="card-title">Data Penilaian oleh Tim Ternormalisasi</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    @foreach(Session::get('dataObjectiveNormalized')[0] as $key => $item)
                                        <th class="p-2">C{{ $key + 1 }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Session::get('dataObjectiveNormalized') as $item)
                                    <tr>
                                        @foreach($item as $value)
                                            <td class="p-2 text-right">{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex justify-content-between mt-4">
                            <h4 class="card-title">Matriks Perbandingan Berpasangan</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2">Kriteria</th>
                                    @foreach(Session::get('dataPairwiseComparison')[0] as $key => $item)
                                        <th class="p-2">C{{ $key + 1 }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Session::get('dataPairwiseComparison') as $key => $item)
                                    <tr>
                                        <td class="p-2 bg-dark text-white" style="width: 20px;"><b>C{{ $key + 1 }}</b></td>
                                        @foreach($item as $value)
                                            <td class="p-2 text-right" style="max-width: 20px;">{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex justify-content-between align-items-center mt-4" style="gap: 8px;">
                            <h4 class="card-title">V / Kromosom</h4>
                            <pre class="flex-grow-1">{{ Session::get('chromosome') }}</pre>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2">#</th>
                                    <th class="p-2">Gen</th>
                                    <th class="p-2">Nilai Gen</th>
                                    <th class="p-2">xi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Session::get('gens') as $key => $item)
                                    <tr>
                                        <td class="p-2" style="width: 20px;"><b>V{{ $key + 1 }}</b></td>
                                        <td class="p-2">{{ $item['slice'] }}</td>
                                        <td class="p-2">{{ $item['value'] }}</td>
                                        <td class="p-2">{{ $item['xi'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex justify-content-between mt-4">
                            <h4 class="card-title">Bobot</h4>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" style="width: 100%;">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="p-2">Kriteria</th>
                                    <th class="p-2">Bobot</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Session::get('dataWeight') as $key => $value)
                                    <tr>
                                        <td class="p-2"><b>C{{ $key + 1 }}</b></td>
                                        <td class="p-2">{{ $value }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-publish').click(function () {
                const id = $(this).data('id');
                const weight = $(this).data('weight');
                console.log(weight);
                Swal.fire({
                    title: 'Generate Bobot?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Generate',
                    cancelButtonText: 'Tutup'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = '{{ route('bobot-kriteria-generate', ["id" => ':id']) }}'.replace(':id', id);
                        // generateBobot(id);
                    }
                });
            });

            function generateBobot(id) {
                $.ajax({
                    url: '{{ route('bobot-kriteria-generate', ["id" => ':id']) }}'.replace(':id', id),
                    type: 'GET',
                    contentType: 'application/json',
                    success: function (response) {
                        console.log(response);
                        if (response.success) {
                            console.log(response.data.reduce(function (result, val) {
                                return result + val;
                            }, 0));
                            $.toast({
                                heading: 'Success',
                                text: 'Bobot berhasil diupdate.',
                                showHideTransition: 'slide',
                                icon: 'success',
                                position: 'top-right'
                            });
                        }
                    },
                    error: function (err) {
                        $.toast({
                            text: err.responseJSON.message,
                            showHideTransition: 'slide',
                            icon: 'error',
                            position: 'top-right'
                        });
                    }
                });
            }
        });
    </script>
@endpush
