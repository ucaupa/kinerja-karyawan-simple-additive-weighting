@extends('layouts.app')

@section('title', 'Tambah Kriteria Penilaian')

@section('css-plugins')
@endsection

@section('styles')
    <style>
    </style>
@endsection

@section('content')
    <div class="row mt-3">
        <div class="col-lg-8 offset-lg-2">
            @include('pages.others.form')
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $('.textarea-height-auto').each(function (key, value) {
                value.style.overflow = 'hidden';
                value.style.height = 0;
                value.style.height = value.scrollHeight + 'px';
            });
        });

        $(document).on('keydown keyup keypress', '.textarea-height-auto', function () {
            this.style.overflow = 'hidden';
            this.style.height = 0;
            this.style.height = this.scrollHeight + 'px';
        });
    </script>
@endpush
