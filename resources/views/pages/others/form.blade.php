<div class="w-100 d-flex flex-column">
    <div class="card mb-3">
        <div class="card-body">
            <div>
                <p class="card-title">{{ $assessment->title }}</p>
                <p style="line-height: 1rem;">{!! nl2br($assessment->description) !!}</p>
                @if(!empty($userAssessment))
                    <hr>
                    <div class="d-flex justify-content-between">
                        <div class="font-weight-bold">{{ $userAssessment->name }}</div>
                        <div class="text-small text-linkedin">{{ $userAssessment->email }}</div>
                    </div>
                @endif
                <hr>
                <p class="text-danger mb-0">* Menunjukkan pertanyaan yang wajib diisi</p>
            </div>
        </div>
    </div>

    <div class="container-items">
        @foreach($assessment->items as $key => $item)
            <div class="card card-section mb-3">
                <div class="card-body">
                    @if($item->field_type == \App\Constants\Fields::TITLE)
                        <p class="card-title mb-0" style="font-size: 14px">{{ $item->title }}</p>
                    @else
                        <p class="mb-0" style="font-size: 14px">
                            "{{ $item->title }}" <span class="text-danger">*</span>
                        </p>
                    @endif
                    @if($item->description)
                        <p class="mb-0 mt-3" style="font-size: 14px">{!! nl2br($item->description) !!}</p>
                    @endif
                    <div class="row mt-3">
                        @if($item->field_type == \App\Constants\Fields::INPUT_LINEAR_SCALE)
                            <div
                                class="col-sm-12 d-flex flex-row align-items-end justify-content-start">
                                @php
                                    [$value1, $value2] = json_decode($item->field_value, true);
                                @endphp
                                @if($value1['label'])
                                    <div class="mr-2 mr-sm-4">{{ $value1['label'] }}</div>
                                @endif
                                @for($i = $value1['value']; $i <= $value2['value']; $i++)
                                    <div
                                        class="custom-control custom-radio d-flex flex-column align-items-center justify-content-center px-1 px-sm-3 pb-0">
                                        <label for="radio-{{ $key . $i }}">
                                            {{ $i }}
                                        </label>
                                        <input type="radio"
                                               name="input_{{ $item->id }}"
                                               id="radio-{{ $key . $i }}"
                                               value="{{ $i }}"
                                               required>
                                    </div>
                                @endfor
                                @if($value2['label'])
                                    <div class="ml-2 ml-sm-4">{{ $value2['label'] }}</div>
                                @endif
                            </div>
                        @elseif($item->field_type == \App\Constants\Fields::INPUT_NUMBER)
                            <div class="col-sm-7 form-group order-2 order-sm-1">
                                <input type="number"
                                       name="input_{{ $item->id }}"
                                       class="form-control mb-0"
                                       required
                                       placeholder="Jawaban anda">
                            </div>
                        @elseif($item->field_type == \App\Constants\Fields::INPUT_PARAGRAPH)
                            <div class="col-sm-12 form-group">
                                                <textarea
                                                    name="input_{{ $item->id }}"
                                                    class="form-control textarea-height-auto"
                                                    required
                                                    placeholder="Jawaban anda"
                                                    rows="1"></textarea>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
