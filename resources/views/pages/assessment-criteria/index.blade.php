@extends('layouts.app')

@section('title', 'Kriteria Penilaian')

@section('css-plugins')
@endsection

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="offset-sm-6 col-sm-6">
            <div class="d-flex align-items-center justify-content-end">
                <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i
                            class="typcn typcn-arrow-forward-outline mr-2"></i>Import
                    </button>
                </div>
                <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i
                            class="typcn typcn-info-large-outline mr-2"></i>info
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3">
                        <h4 class="card-title">Kriteria Penilaian</h4>
                        <a type="button" href="{{ route('kriteria-penilaian-create') }}" class="btn btn-outline-info">
                            Tambah Data
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-sm">
                            <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Tim Penilai</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td colspan="4" class="text-center">Belum ada data</td>
                                </tr>
                            @endif
                            @foreach($data as $item)
                                @php
                                    $needReview = false;
                                @endphp
                                <tr>
                                    <td class="valign-top" style="width: 300px;">
                                        <b>{{ $item->title }}</b>
                                        @if($item->description)
                                            <div class="text-ellipsis-4">{{ $item->description }}</div>
                                        @endif
                                    </td>
                                    <td class="valign-top">
                                        @if($item->teams)
                                            <ul class="m-0">
                                                @foreach($item->teams as $team)
                                                    <li>
                                                        {{ $team->user->name }}
                                                        @if($item->status > \App\Constants\AssessmentStatus::PENDING)
                                                            @if($item->items_count == count($team->reviews))
                                                                <i class="typcn typcn-tick text-success my-auto"
                                                                   style="cursor: help; font-size: 20px;"
                                                                   title="Sudah Menilai"></i>
                                                            @else
                                                                @php
                                                                    if (auth()->id() == $team->user_id && $item->status == \App\Constants\AssessmentStatus::REVIEW_BY_TEAMS) {
                                                                        $needReview = true;
                                                                    }
                                                                @endphp
                                                            @endif
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @else
                                            Kosong
                                        @endif
                                    </td>
                                    <td class="valign-top">
                                        <p>Dibuat oleh: {{ $item->createBy->name }}</p>
                                        <p>Terakhir update: {{ $item->updated_at }}</p>
                                        @if($item->status == \App\Constants\AssessmentStatus::PENDING)
                                            <label class="badge badge-outline-danger">
                                                {{ \App\Constants\AssessmentStatus::LIST_OPTIONS[$item->status] }}
                                            </label>
                                        @elseif($item->status == \App\Constants\AssessmentStatus::REVIEW_BY_TEAMS)
                                            <label class="badge badge-danger">
                                                {{ \App\Constants\AssessmentStatus::LIST_OPTIONS[$item->status] }}
                                            </label>
                                        @elseif($item->status == \App\Constants\AssessmentStatus::MAPPING_USERS)
                                            <label class="badge badge-warning">
                                                {{ \App\Constants\AssessmentStatus::LIST_OPTIONS[$item->status] }}
                                            </label>
                                        @elseif($item->status == \App\Constants\AssessmentStatus::PUBLISHED)
                                            <label class="badge badge-info">
                                                {{ \App\Constants\AssessmentStatus::LIST_OPTIONS[$item->status] }}
                                            </label>
                                        @endif
                                    </td>
                                    <td class="valign-top d-flex flex-column">
                                        <a href="{{ route('kriteria-penilaian-show', ['id' => $item->id]) }}"
                                           class="btn btn-info btn-sm"
                                           style="font-size: 12px">
                                            Edit
                                        </a>
                                        @if($item->status > \App\Constants\AssessmentStatus::PENDING)
                                            <a href="{{ route('kriteria-penilaian-preview-show', ['id' => $item->id]) }}"
                                               class="btn btn-outline-success btn-sm mt-2"
                                               style="font-size: 12px">
                                                Preview
                                            </a>
                                        @endif
                                        @if($needReview)
                                            <a href="{{ route('kriteria-penilaian-review-show', ['id' => $item->id]) }}"
                                               class="btn btn-warning btn-sm mt-2 text-white"
                                               style="font-size: 12px">
                                                Beri Penilaian
                                            </a>
                                        @endif
                                        @if($item->status == \App\Constants\AssessmentStatus::MAPPING_USERS)
                                            <button type="button"
                                                    class="btn btn-success btn-sm mt-2 btn-publish"
                                                    style="font-size: 12px"
                                                    data-id="{{ $item->id }}">
                                                Publikasi
                                            </button>
                                        @endif
                                        @if($item->status == \App\Constants\AssessmentStatus::PUBLISHED)
                                            <button type="button"
                                                    class="btn btn-outline-danger btn-sm mt-2 btn-un-publish"
                                                    style="font-size: 12px"
                                                    data-id="{{ $item->id }}">
                                                Batal Publikasi
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-publish').click(function () {
                const id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: 'Setelah penilaian ini dipublikasikan, anda tidak dapat menambahkan kembali kriteria baru.',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Publikasi',
                    cancelButtonText: 'Tutup'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = '{{ route('kriteria-penilaian-publish', ['id' => ':id']) }}'.replace(':id', id);
                    }
                });
            });
            $('.btn-un-publish').click(function () {
                const id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: 'Penilaian Alternatif yang sebelumnya telah dilakukan, akan ter-reset.',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Batal Publikasi',
                    cancelButtonText: 'Tutup'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location = '{{ route('kriteria-penilaian-unpublish', ['id' => ':id']) }}'.replace(':id', id);
                    }
                });
            });
        });
    </script>
@endpush
