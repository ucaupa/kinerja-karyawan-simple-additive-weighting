@extends('layouts.app')

@section('title', 'Tambah Kriteria Penilaian')

@section('css-plugins')
@endsection

@section('styles')
    <style>
        .form-group {
            margin-bottom: 5px;
        }

        button:disabled,
        button[disabled] {
            cursor: not-allowed;
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('kriteria-penilaian-store') }}" method="POST" id="form-input">
        @csrf
        <div class="row">
            <div class="offset-sm-6 col-sm-6">
                <div class="d-flex align-items-center justify-content-end">
                    <div class="pr-1 mb-3 mr-2 mb-xl-0">
                        <button type="button" class="btn btn-info btn-sm btn-save-preview">
                            Simpan & Lihat Preview
                        </button>
                        <button type="button" class="btn btn-primary btn-sm ml-3 btn-save">
                            Simpan
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-8 offset-lg-2">
                <div class="w-100 d-flex flex-column">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div>
                                <div class="form-group">
                                    <input
                                        type="text"
                                        name="title"
                                        class="form-control card-title mb-0"
                                        required
                                        value="{{ $assessment->title }}"
                                        placeholder="Judul Penilaian">
                                </div>
                                <div class="form-group">
                                    <textarea
                                        name="description"
                                        class="form-control textarea-height-auto"
                                        placeholder="Deskripsi Penilaian"
                                        required
                                        rows="1">{{ $assessment->description }}</textarea>
                                </div>
                                @php
                                    $isAllowEditItem = $assessment->status < \App\Constants\AssessmentStatus::PUBLISHED;
                                @endphp
                                <div class="form-group mt-3">
                                    <label>Tim Penilai</label>
                                    <select class="select-teams" name="teams[]" multiple required style="width: 100%;">
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <p class="text-right text-small mt-3">
                                    Dibuat oleh: {{ $assessment->createBy->name }}
                                </p>

                                <hr>
                                <div class="d-flex flex-row justify-content-end gap-4">
                                    <div class="dropdown">
                                        <button
                                            type="button"
                                            class="btn btn-inverse-info btn-rounded btn-icon"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false" {{ !$isAllowEditItem ? 'disabled' : '' }}>
                                            <i class="typcn typcn-plus"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"
                                             x-placement="bottom-start"
                                             style="position: absolute; transform: translate3d(-31px, 47px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <button type="button" class="dropdown-item btn-add-question-1">
                                                Tambahkan Kriteria Penilaian
                                            </button>
                                            <button type="button" class="dropdown-item btn-add-section-1">
                                                Tambahkan Judul & Deskripsi
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-items" style="display: none;"></div>
                    <div class="container-items-empty text-center">
                        <div class="card">
                            <div class="card-body">
                                <p class="m-0 text-gray">Belum ada Kriteria</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection


@push('scripts')
    <script>
        let fields = JSON.parse('{!! json_encode(\App\Constants\Fields::LIST_OPTIONS) !!}');
        let dataItems = @json($assessment->items);
        $(document).ready(function () {
            let containerItems = $('.container-items');
            containerItems.sortable({});
            let selectTeams = $(".select-teams");
            $(selectTeams).select2({
                disabled: '{{ !$isAllowEditItem ? 'readonly' : '' }}'
            });
            $(selectTeams).val({{ json_encode($selectedTeams) }}).trigger('change');

            $('.textarea-height-auto').each(function (key, value) {
                value.style.overflow = 'hidden';
                value.style.height = 0;
                value.style.height = value.scrollHeight + 'px';
            });

            dataItems.forEach(function (item) {
                let html = buildItemCard(item.field_type, item);
                $(containerItems).append(html);
            });
            if (dataItems.length > 0) {
                $('.container-items-empty').hide();
                $(containerItems).show();
            }
        });

        $(document).on('keydown keyup keypress', '.textarea-height-auto', function () {
            this.style.overflow = 'hidden';
            this.style.height = 0;
            this.style.height = this.scrollHeight + 'px';
        });

        $(document).on('change', '.input-field-type', function () {
            let html = buildItemQuestion($(this).val());
            $(this).closest('.card-section').find('.content-field').html(html);
            $(this).closest('.card-section').find('.item-type').val($(this).val());
        });

        $(document).on('change', '.select-input-from', function () {
            let val = $(this).val();
            $(this).closest('.content-field').find('.label-input-from').html(val);
        });

        $(document).on('change', '.select-input-to', function () {
            let val = $(this).val();
            $(this).closest('.content-field').find('.label-input-to').html(val);
        });

        $(document).on('change', '.item-group-checkbox', function () {
            let checked = $(this).is(':checked');
            $(this).closest('.card-section').find('.item-group').val('');
            if (checked) {
                $(this).closest('.card-section').find('.container-item-group').show();
            } else {
                $(this).closest('.card-section').find('.container-item-group').hide();
            }
        });

        $(document).on('click', '.btn-add-question-1', function () {
            let html = buildItemCard('{{ \App\Constants\Fields::INPUT_LINEAR_SCALE }}');
            let containerItem = $('.container-items')
            if (containerItem.html() === '') {
                $('.container-items-empty').hide();
                containerItem.show();
            }
            containerItem.prepend(html);
        });
        $(document).on('click', '.btn-add-section-1', function () {
            let html = buildItemCard('{{ \App\Constants\Fields::TITLE }}');
            let containerItem = $('.container-items')
            if (containerItem.html() === '') {
                $('.container-items-empty').hide();
                containerItem.show();
            }
            containerItem.prepend(html);
        });
        $(document).on('click', '.btn-add-question', function () {
            let html = buildItemCard('{{ \App\Constants\Fields::INPUT_LINEAR_SCALE }}');
            $(this).closest('.card-section').after(html);
        });
        $(document).on('click', '.btn-add-section', function () {
            let html = buildItemCard('{{ \App\Constants\Fields::TITLE }}');
            $(this).closest('.card-section').after(html);
        });
        $(document).on('click', '.btn-delete', function () {
            $(this).closest('.card-section').remove();
            let containerItem = $('.container-items')
            if (containerItem.html() === '') {
                $('.container-items-empty').show();
                containerItem.hide();
            }
        });
        /*$(document).on('click', '.btn-copy', function () {
            console.log('copied')
        });*/

        $(document).on('click', '.btn-save-preview', function () {
            submitData(true)
        });
        $(document).on('click', '.btn-save', function () {
            submitData();
        });

        function submitData(preview = false) {
            let countCriteria = 0;
            $('.item-type').each(function (key, value) {
                let val = $(value).val();
                if (['{{ \App\Constants\Fields::INPUT_LINEAR_SCALE }}', '{{ \App\Constants\Fields::INPUT_NUMBER }}'].includes(val)) countCriteria++;
            });
            if (countCriteria < 3) {
                $.toast({
                    heading: 'Warning',
                    text: 'Tambahkan kriteria, minimal 3',
                    showHideTransition: 'plain',
                    icon: 'warning',
                    position: 'top-right',
                    bgColor: 'white',
                    textColor: 'black'
                });
            }
            let form = $('#form-input');
            if (!$(form)[0].checkValidity()) {
                $(form)[0].reportValidity();
                return;
            }
            if (countCriteria < 3) {
                return;
            }

            let items = [];
            $('.card-section').each(function (key, value) {
                let itemId = $(value).find('.item-id').val();
                let itemType = $(value).find('.item-type').val();
                let itemValue = null;
                let checked = $(value).find('.item-group-checkbox').is(':checked');
                let itemGroup = checked ? $(value).find('.item-group').val() : null;
                let isBenefit = $(value).find('.ck-cost-benefit').is(':checked');
                let itemCostBenefit = null;
                if (isBenefit) itemCostBenefit = 1;
                else if (itemType === '{{ \App\Constants\Fields::TITLE }}' || itemType === '{{ \App\Constants\Fields::INPUT_PARAGRAPH }}') itemCostBenefit = null;
                else itemCostBenefit = 0;
                if (itemType === '{{ \App\Constants\Fields::INPUT_LINEAR_SCALE }}') {
                    itemValue = [];
                    itemValue.push({
                        value: parseInt($(value).find('.select-input-from').val(), 10),
                        label: $(value).find('.input-from').val(),
                    });
                    itemValue.push({
                        value: parseInt($(value).find('.select-input-to').val(), 10),
                        label: $(value).find('.input-to').val(),
                    });
                }
                let item = {
                    id: itemId,
                    title: $(value).find('.item-title').val(),
                    description: $(value).find('.item-description').val(),
                    type: itemType,
                    value: itemValue,
                    group: itemGroup,
                    cost_benefit: itemCostBenefit,
                };
                items.push(item);
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ route('kriteria-penilaian-update', ["id" => $assessment->id]) }}',
                type: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify({
                    title: $('input[name=title]').val(),
                    description: $('textarea[name=description]').val(),
                    teams: $('.select-teams').val(),
                    items,
                }),
                success: function (response) {
                    if (response.success) {
                        $.toast({
                            heading: 'Success',
                            text: 'Data berhasil disimpan',
                            showHideTransition: 'slide',
                            icon: 'success',
                            position: 'top-right'
                        });

                        setTimeout(function () {
                            if (preview) {
                                window.location = '{{ route('kriteria-penilaian-preview-show', ['id' => $assessment->id]) }}'
                            } else {
                                window.location = '{{ route('kriteria-penilaian') }}'
                            }
                        }, 1000)
                    }
                },
                fail: function (err) {
                    console.log(err);
                }
            });
        }

        let seqId = 0;

        function buildItemCard(type, data = {}) {
            seqId++;
            let htmlQuestion = buildItemQuestion(type, data);
            let titleLabel = type !== '{{ \App\Constants\Fields::TITLE }}' ? 'Kriteria' : 'Judul';
            let itemId = data.id || '';
            let itemValue = data.title || '';
            let itemDescription = data.description || '';
            let isHasItemGroup = !!data.group;
            let itemGroup = data.group;
            let isItemBenefit = !!data.cost_benefit;
            let ckCostBenefit = '';
            if (isItemBenefit || !itemId) ckCostBenefit = 'checked';
            let ckGroup = '';
            if (isHasItemGroup) ckGroup = 'checked';
            let options = '';
            Object.keys(fields).forEach(function (key) {
                if (key !== '{{ \App\Constants\Fields::TITLE }}') {
                    let selected = type === key ? 'selected' : '';
                    options += '<option value="' + key + '" ' + selected + '>' + fields[key] + '</option>';
                }
            });
            let htmlSelectType = '';
            let html = '';
            let htmlFooterLeft = '';
            if (type !== '{{ \App\Constants\Fields::TITLE }}') {
                htmlSelectType = '' +
                    '                <div class="col-sm-5 form-group order-1 order-sm-2">' +
                    '                    <select class="form-control h-100 input-field-type">' +
                    options +
                    '                    </select>' +
                    '                </div>';
                html = '' +
                    '           <div class="row">' +
                    '                <div class="col-sm-12 form-group order-3">' +
                    '                    <div class="custom-control custom-checkbox">' +
                    '                       <input type="checkbox" class="custom-control-input ck-cost-benefit" id="ck-cost-benefit-' + seqId + '" ' + ckCostBenefit + '>' +
                    '                       <label class="custom-control-label" for="ck-cost-benefit-' + seqId + '">Benefit / Keuntungan (centang jika nilai kriteria yang lebih tinggi dianggap lebih baik)</label>' +
                    '                    </div>' +
                    '                </div>' +
                    '                <div class="col-12 form-group order-4 container-item-group" style="' + (isHasItemGroup ? '' : 'display: none;') + '">' +
                    '                    <input type="text"' +
                    '                           name="group"' +
                    '                           class="form-control mb-0 item-group"' +
                    '                           value="' + itemGroup + '"' +
                    '                           placeholder="Kelompok Kriteria (optional)">' +
                    '                    <small class="form-text text-muted">Jika diisi maka saat penilaian akhir kriteria penilaian ini akan di gabungkan dengan kelompok serupa.</small>' +
                    '                </div>' +
                    '           </div>';
                // htmlFooterLeft = '';
                    // '                <div class="d-flex flex-row align-items-center">' +
                    // '                    <div class="custom-control custom-checkbox">' +
                    // '                       <input type="checkbox" class="custom-control-input item-group-checkbox" id="ck-group-' + seqId + '" ' + ckGroup + '>' +
                    // '                       <label class="custom-control-label" for="ck-group-' + seqId + '">Aktifkan Kelompok</label>' +
                    // '                    </div>' +
                    // '                </div>' +
                    // '                <div class="border-right mx-3"></div>';
            }
            const disableAllowEditItem = '{{ !$isAllowEditItem ? "disabled" : "" }}';
            return '' +
                '<div class="card card-section mb-3">' +
                '    <div class="card-body pt-0 px-0">' +
                '        <div class="text-center py-3 text-gray cursor-move">' +
                '            <i class="typcn typcn-th-small"></i>' +
                '        </div>' +
                '        <div class="px-4">' +
                '            <input type="hidden" class="item-id" value="' + itemId + '">' +
                '            <input type="hidden" class="item-type" value="' + type + '">' +
                '            <div class="row">' +
                '                <div class="col-sm-7 form-group order-2 order-sm-1">' +
                '                    <input type="text"' +
                '                           class="form-control mb-0 h-100 item-title"' +
                '                           required' +
                '                           value="' + itemValue + '"' +
                '                           placeholder="' + titleLabel + '">' +
                '                </div>' +
                htmlSelectType +
                '            </div>' +
                '            <div class="form-group">' +
                '                <textarea' +
                '                    class="form-control textarea-height-auto item-description"' +
                '                    placeholder="Deskripsi ' + titleLabel + ' (optional)"' +
                '                    rows="1">' + itemDescription + '</textarea>' +
                '            </div>' +
                html +
                '' +
                (htmlQuestion !== '' ? '            <hr>' : '') +
                '            <div class="content-field">' + htmlQuestion + '</div>' +
                '' +
                '            <hr>' +
                '            <div class="d-flex flex-row justify-content-end gap-4">' +
                htmlFooterLeft +
                // '                <button type="button" class="btn btn-inverse-light btn-rounded btn-icon btn-copy">' +
                // '                    <i class="typcn typcn-tabs-outline"></i>' +
                // '                </button>' +
                '                <button type="button" class="btn btn-inverse-danger btn-rounded btn-icon btn-delete" ' + disableAllowEditItem + '>' +
                '                    <i class="typcn typcn-trash"></i>' +
                '                </button>' +
                '                <div class="dropdown">' +
                '                    <button' +
                '                        type="button"' +
                '                        class="btn btn-inverse-info btn-rounded btn-icon"' +
                '                        data-toggle="dropdown"' +
                '                        aria-haspopup="true"' +
                '                        aria-expanded="false" ' + disableAllowEditItem + '>' +
                '                        <i class="typcn typcn-plus"></i>' +
                '                    </button>' +
                '                    <div class="dropdown-menu dropdown-menu-right"' +
                '                         x-placement="bottom-start"' +
                '                         style="position: absolute; transform: translate3d(-31px, 47px, 0px); top: 0px; left: 0px; will-change: transform;">' +
                '                        <button type="button" class="dropdown-item btn-add-question">Tambahkan Kriteria Penilaian</button>' +
                '                        <button type="button" class="dropdown-item btn-add-section">Tambahkan Judul & Deskripsi</button>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '</div>';

        }

        function buildItemQuestion(type, data = {}) {
            if (type === '{{ \App\Constants\Fields::INPUT_LINEAR_SCALE }}') {
                let itemValues = data.field_value ? JSON.parse(data.field_value) : [];
                let [val1, val2] = itemValues;
                let options = '';
                let valueOption1 = !!val1 ? val1.value : 1;
                let valueOption2 = !!val2 ? val2.value : 5;
                for (let i = 2; i <= 7; i++) {
                    let checkSelected = valueOption2 === i ? 'selected' : '';
                    options += '<option value="' + i + '"' + checkSelected + '>' + i + '</option>';
                }
                return '' +
                    '<div class="row">' +
                    '   <div class="col-4 col-sm-2 form-group">' +
                    '       <select class="form-control select-input-from">' +
                    '           <option value="0" ' + (valueOption1 === 0 ? 'selected' : '') + '>0</option>' +
                    '           <option value="1" ' + (valueOption1 === 1 ? 'selected' : '') + '>1</option>' +
                    '       </select>' +
                    '   </div>' +
                    '   <div class="my-auto text-center">' +
                    '       sampai' +
                    '   </div>' +
                    '   <div class="col-4 col-sm-2 form-group">' +
                    '       <select class="form-control select-input-to">' +
                    options +
                    '       </select>' +
                    '   </div>' +
                    '</div>' +
                    '<div class="row">' +
                    '    <div class="col-12">' +
                    '        <div' +
                    '           class="custom-control custom-control-inline align-items-center mt-2">' +
                    '           <label class="mr-sm-3 my-auto label-input-from">' + valueOption1 + '</label>' +
                    '           <input' +
                    '               type="text"' +
                    '               class="form-control form-control-sm input-from"' +
                    '               placeholder="Label (optional)"' +
                    '               value="' + (val1 && val1.label ? val1.label : '') + '"/>' +
                    '        </div>' +
                    '   </div>' +
                    '   <div class="col-12">' +
                    '       <div' +
                    '           class="custom-control custom-control-inline align-items-center mt-2">' +
                    '           <label class="mr-sm-3 my-auto label-input-to">' + valueOption2 + '</label>' +
                    '           <input' +
                    '               type="text"' +
                    '               class="form-control form-control-sm input-to"' +
                    '               placeholder="Label (optional)"' +
                    '               value="' + (val2 && val2.label ? val2.label : '') + '"/>' +
                    '       </div>' +
                    '   </div>' +
                    '</div>';
            } else if (type === '{{ \App\Constants\Fields::INPUT_NUMBER }}') {
                return '' +
                    '<div class="row">' +
                    '   <div class="form-group col-12 col-sm-5">' +
                    '       <input type="number" class="form-control" placeholder="Teks input angka" disabled/>' +
                    '   </div>' +
                    '</div>';
            } else if (type === '{{ \App\Constants\Fields::INPUT_PARAGRAPH }}') {
                return '' +
                    '<div class="row">' +
                    '   <div class="form-group col-12 col-sm-10">' +
                    '       <textarea' +
                    '           class="form-control textarea-height-auto"' +
                    '           placeholder="Teks jawaban panjang"' +
                    '           rows="1" disabled></textarea>' +
                    '   </div>' +
                    '</div>';
            }
            return '';
        }
    </script>
@endpush
