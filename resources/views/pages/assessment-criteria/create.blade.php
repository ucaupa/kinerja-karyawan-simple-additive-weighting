@extends('layouts.app')

@section('title', 'Tambah Kriteria Penilaian')

@section('css-plugins')
@endsection

@section('styles')
    <style>
        .form-group {
            margin-bottom: 10px;
        }

        .cursor-move {
            cursor: move;
        }
    </style>
@endsection

@section('content')
    <form action="{{ route('kriteria-penilaian-store') }}" method="POST">
        @csrf
        <div class="row mt-3">
            <div class="col-lg-8 offset-lg-2">
                <div class="w-100 d-flex flex-column">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <input
                                    type="text"
                                    name="title"
                                    class="form-control card-title mb-0"
                                    required
                                    placeholder="Judul Penilaian">
                            </div>
                            <div class="form-group">
                            <textarea
                                name="description"
                                class="form-control textarea-height-auto"
                                placeholder="Deskripsi Penilaian"
                                rows="1"></textarea>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label>Tim Penilai</label>
                                <select class="select-teams w-100" name="teams[]" multiple required>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="text-right mt-4">
                        <button class="btn btn-inverse-primary">
                            Simpan
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $(".select-teams").select2();

            $('.textarea-height-auto').each(function (key, value) {
                value.style.overflow = 'hidden';
                value.style.height = 0;
                value.style.height = value.scrollHeight + 'px';
            });
        });

        $(document).on('keydown keyup keypress', '.textarea-height-auto', function () {
            this.style.overflow = 'hidden';
            this.style.height = 0;
            this.style.height = this.scrollHeight + 'px';
        });
    </script>
@endpush
