@extends('layouts.app')

@section('title', 'Mapping Alternatif')

@section('css-plugins')
@endsection

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="offset-sm-6 col-sm-6">
            <div class="d-flex align-items-center justify-content-end">
                <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i
                            class="typcn typcn-arrow-back-outline mr-2"></i>Import
                    </button>
                </div>
                <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i
                            class="typcn typcn-info-large-outline mr-2"></i>info
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3">
                        <h4 class="card-title">List Penilaian</h4>
                    </div>

                    <div class="d-flex justify-content-start mb-3">
                        <div class="form-row w-100">
                            <div class="form-group col-md-4">
                                <label for="filter-assessment">Judul Kriteria Penilaian</label>
                                <select class="form-control select2 select2-filter" id="filter-assessment"
                                        style="width: 100%;">
                                    <option></option>
                                    @foreach($assessments as $assessment)
                                        <option value="{{ $assessment->id }}">{{ $assessment->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4 d-none">
                                <label for="filter-user">
                                    Alternatif <small class="text-danger">(orang yang dinilai)</small>
                                </label>
                                <select class="form-control select2 select2-filter" id="filter-user"
                                        style="width: 100%;">
                                    <option></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="filter-evaluator">
                                    Penilai Alternatif <small class="text-danger">(orang yang menilai)</small>
                                </label>
                                <select class="form-control select2 select2-filter" id="filter-evaluator"
                                        style="width: 100%;">
                                    <option></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="table-responsive">
                        <table class="table table-hover table-sm" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Alternatif</th>
                                <th>Penilai</th>
                                <th width="100px">Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade"
         id="modal-detail"
         data-backdrop="static"
         data-keyboard="false"
         tabindex="-1"
         aria-labelledby="staticBackdropLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <form action="" id="form-input">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="detail-items"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $(".select2").select2();
            let select2Filter = $(".select2-filter").select2();
            $("#filter-assessment").select2({
                allowClear: true,
                placeholder: 'Pilih Kriteria Penilaian'
            });
            $("#filter-user").select2({
                allowClear: true,
                placeholder: 'Pilih Alternatif'
            });
            $("#filter-evaluator").select2({
                allowClear: true,
                placeholder: 'Pilih Penilai Alternatif'
            });

            let table = $('table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('penilaian-alternatif-index') }}',
                    data: function (d) {
                        d.assessment_id = $('#filter-assessment').val();
                        d.evaluator_id = $('#filter-evaluator').val();
                        d.user_id = $('#filter-user').val();
                    }
                },
                columns: [
                    {data: 'assessment.title', name: 'assessment.title'},
                    {data: 'name', name: 'name'},
                    // {data: 'email', name: 'email'},
                    {data: 'evaluator_name', name: 'evaluator_name'},
                    // {data: 'evaluator_email', name: 'evaluator_email'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $(select2Filter).change(function () {
                table.draw();
            });

            $(document).on('click', '.btn-detail', function () {
                let data_row = table.row($(this).closest('tr')).data();
                console.log(data_row);
                let html = '<ul>';
                $.each(data_row.reviews, function (key, item) {
                    html += '<li>' + item.item.title + ':' + item.field_value + '</li>';
                });
                html += '</ul>'
                $('.detail-items').html(html);
                $('#modal-detail').modal('show');
            });
        });
    </script>
@endpush
