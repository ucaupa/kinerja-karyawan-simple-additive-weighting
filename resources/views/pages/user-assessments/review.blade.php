@extends('layouts.app')

@section('title', 'Tambah Kriteria Penilaian')

@section('css-plugins')
@endsection

@section('styles')
    <style>
    </style>
@endsection

@section('content')
    <form
        action="{{ route('penilaian-alternatif-update', ['assessmentId' => request()->route('assessmentId'), 'userId' => request()->route('userId')]) }}"
        method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="offset-sm-6 col-sm-6">
                <div class="d-flex align-items-center justify-content-end">
                    <div class="pr-1 mb-3 mr-2 mb-xl-0">
                        <button type="submit" class="btn btn-primary btn-sm ml-3 btn-save">
                            Simpan
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-8 offset-lg-2">
                @include('pages.others.form')
            </div>
        </div>
    </form>
@endsection


@push('scripts')
@endpush
