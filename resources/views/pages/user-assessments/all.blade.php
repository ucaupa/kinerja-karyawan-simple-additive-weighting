@extends('layouts.app')

@section('title', 'Mapping Alternatif')

@section('css-plugins')
@endsection

@section('styles')
    <style>
        .group {
            cursor: pointer;
        }

        .group td {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .group td:first-child {
            font-weight: bold;
            /*background: #f3f3f3;*/
        }

        .group-children {
            background: #f3f3f3 !important;
            display: none;
        }

        .group-children td {
            border: 0;
        }

        .group-children td:first-child {
            padding-left: 15px;
        }

        .group-children td:first-child:before {
            content: "- ";
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="offset-sm-6 col-sm-6">
            <div class="d-flex align-items-center justify-content-end">
                <div class="pr-1 mb-3 mr-2 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i
                            class="typcn typcn-arrow-back-outline mr-2"></i>Import
                    </button>
                </div>
                <div class="pr-1 mb-3 mb-xl-0">
                    <button type="button" class="btn btn-sm bg-white btn-icon-text border"><i
                            class="typcn typcn-info-large-outline mr-2"></i>info
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3">
                        <h4 class="card-title">List Penilaian</h4>
                    </div>

                    <div class="d-flex justify-content-start mb-3">
                        <form action="{{ route('response-penilaian-index') }}"
                              class="form-row w-100 d-flex flex-column flex-sm-row justify-content-start align-items-end"
                              id="form-filter">
                            <div class="form-group col-md-4">
                                <label for="filter-assessment">Judul Kriteria Penilaian</label>
                                <select class="form-control select2 select2-filter" id="filter-assessment"
                                        name="assessment"
                                        required
                                        style="width: 100%;">
                                    <option></option>
                                    @foreach($assessments as $assessment)
                                        <option value="{{ $assessment->id }}">{{ $assessment->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="filter-user">
                                    Alternatif <small class="text-danger">(orang yang dinilai)</small>
                                </label>
                                <select class="form-control select2 select2-filter" id="filter-user" name="alternatif"
                                        style="width: 100%;">
                                    <option></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4 d-none">
                                <label for="filter-evaluator">
                                    Penilai Alternatif <small class="text-danger">(orang yang menilai)</small>
                                </label>
                                <select class="form-control select2 select2-filter" id="filter-evaluator" name="penilai"
                                        style="width: 100%;">
                                    <option></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4 text-right text-md-left">
                                <button type="button" class="btn btn-primary btn-search">Cari</button>
                            </div>
                        </form>
                    </div>

                    <hr>

                    <div class="table-responsive">
                        <table class="table table-hover table-sm" style="width: 100%;"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $(".select2").select2();
            let select2Filter = $(".select2-filter").select2();
            $("#filter-assessment").select2({
                allowClear: true,
                placeholder: 'Pilih Kriteria Penilaian'
            });
            $("#filter-user").select2({
                allowClear: true,
                placeholder: 'Pilih Alternatif'
            });
            $("#filter-evaluator").select2({
                allowClear: true,
                placeholder: 'Pilih Penilai Alternatif'
            });

            $(document).on('click', '.btn-search', function () {
                let form = $('#form-filter');
                if (!$(form)[0].checkValidity()) {
                    $(form)[0].reportValidity();
                    return;
                }

                /*$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });*/
                let table = $('table');
                $.ajax({
                    url: '{{ route('response-penilaian-index') }}',
                    type: 'GET',
                    contentType: 'application/json',
                    data: {
                        assessment_id: $('#filter-assessment').val(),
                        evaluator_id: $('#filter-evaluator').val(),
                        user_id: $('#filter-user').val(),
                    },
                    success: function (response) {
                        let defs = [
                            {
                                visible: false,
                                targets: 1
                            },
                        ];
                        if ($.fn.DataTable.isDataTable(table)) {
                            $(table).DataTable().clear().destroy();
                            $(table).html('');
                        }
                        let columns = response.columns;
                        let groupColumn = 1;
                        $(table).DataTable({
                            data: response.data,
                            columns,
                            order: [[groupColumn, 'asc']],
                            columnDefs: defs,
                            paging: false,
                            drawCallback: function () {
                                if (groupColumn >= 0) {
                                    // create groups
                                    let api = this.api();
                                    let rows = api.rows({page: 'current'}).nodes();
                                    let last = null;
                                    let groupIndex = 0;
                                    api.column(groupColumn, {page: 'current'}).data().each(function (group, i) {
                                        // debugger;
                                        if (last !== group) {
                                            groupIndex++;
                                            if (groupCallback === undefined) {
                                                $(rows).eq(i).before('<tr class="group" backcolor="' + group + '" data-id="' + groupIndex + '"><td colspan="' + columns.length + '">' + group + '</td></tr>');
                                            } else {
                                                $(rows).eq(i).before(groupCallback(api.data(), defs, groupColumn, group, columns, groupIndex));
                                            }

                                            last = group;
                                        }
                                        $(rows).eq(i).addClass('group-children-' + groupIndex);
                                        $(rows).eq(i).addClass('group-children');
                                    });
                                }
                            },
                        });
                    },
                    fail: function (err) {
                        if ($.fn.DataTable.isDataTable(table)) {
                            $(table).DataTable().clear().destroy();
                            $(table).html('');
                        }
                    }
                });
            });

            function groupCallback(data, defs, groupcolum, group, columns, groupIndex) {
                // prepare array with value for each cell on our group
                let def_row = new Array(columns.length);

                // and get the field for the grouping
                let grp = columns[groupcolum].data;

                // cycle on row; quicker than the opposite
                data.each(function (curRow) {
                    // get the grouping VALUE for this row and check if this row is in this group
                    let groupThisRow = grp.includes('.') ? curRow[grp.split('.')[0]][grp.split('.')[1]] : curRow[grp];
                    if (groupThisRow == group) {

                        // every column of this row
                        for (let col in columns) {
                            let curColumn = columns[col];

                            if (curColumn.aggregate != undefined) {
                                let curValue = curRow[curColumn.data];

                                if (def_row[col] == undefined)
                                    def_row[col] = {count: 0, val: undefined};

                                def_row[col].count++;

                                // perform aggregation on this value
                                switch (curColumn.aggregate) {
                                    case 'max':
                                        if (curValue > def_row[col].val || def_row[col].val == undefined) {
                                            def_row[col].val = curValue;
                                        }
                                        break;

                                    case 'count':
                                        if (def_row[col].val == undefined)
                                            def_row[col].val = 0;

                                        def_row[col].val += 1;
                                        break;

                                    case 'min':
                                        if (curValue < def_row[col].val || def_row[col].val == undefined) {
                                            def_row[col].val = curValue;
                                        }
                                        break;

                                    case 'sum':
                                        if (def_row[col].val == undefined)
                                            def_row[col].val = 0;

                                        def_row[col].val += curValue;
                                        break;

                                    case 'avg':
                                        if (def_row[col].val == undefined)
                                            def_row[col].val = 0;

                                        def_row[col].val += curValue;
                                        break;

                                    case 'first':
                                        if (def_row[col].count == 1)
                                            def_row[col].val = curValue;
                                        break;

                                    case 'last':
                                        def_row[col].val = curValue;
                                        break;

                                    default:
                                        console.log('unknown aggregate: ' + curColumn.aggregate);
                                        break;
                                }
                            } else {
                                // no aggregate for this field
                            }
                        }
                    }
                });

                // now I have the whole array with "RAW" data for each header.
                // I have to create the header for every column
                let s = '', isFirstVisible = true;
                for (let col in columns) {
                    let curColumn = columns[col];

                    // get the definition for this column
                    let curDef = null;
                    for (let d in defs) {
                        let curdef = defs[d];

                        if (curdef.targets == col || (curdef.targets.includes != undefined && curdef.targets.includes(col))) {
                            curDef = curdef;
                            break;
                        }
                    }

                    // for every visible row only (curDef == null because visible is default)
                    if (curDef == null || curDef.visible == null || curDef.visible == true) {
                        // put the group header on the first visible column (only if I don't have to put an aggregate value in that header)
                        if (isFirstVisible && (curColumn.aggregate == undefined || curColumn.aggregate == '')) {
                            s += '<td>' + group + '</td>';
                        } else {
                            // this column has an aggregation?
                            if (curColumn.aggregate != undefined && def_row[col] != undefined) {
                                // process the RAW aggregate
                                if (curColumn.aggregate == 'avg')
                                    def_row[col].val = parseFloat((def_row[col].val / def_row[col].count).toFixed(13));

                                let strThisCell = def_row[col].val;

                                if (curColumn.aggregate != 'count') {
                                    if (curDef != null && curDef.render != undefined) {
                                        strThisCell = curdef.render(strThisCell, "display", null);
                                    }
                                }
                                s += '<td>' + strThisCell + '</td>';
                            } else {
                                // or just put an empty cell
                                s += '<td></td>';
                            }
                        }

                        isFirstVisible = false;
                    }
                }

                return '<tr class="group" backcolor="' + group + '" data-id="' + groupIndex + '">' + s + '</tr>';
            }

            $(document).on('click', '.group', function () {
                let id = $(this).data('id');
                $('.group-children-' + id).toggle();
            });
        });
    </script>
@endpush
