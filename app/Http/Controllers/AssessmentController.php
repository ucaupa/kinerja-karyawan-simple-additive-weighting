<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Constants\Fields;
use App\Models\Assessment;
use App\Models\AssessmentTeam;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class AssessmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Assessment::query()
            ->with(['createBy', 'updateBy', 'teams.user', 'teams.reviews'])
            ->withCount([
                'items' => function ($query) {
                    $query->whereNotIn('field_type', [Fields::TITLE, Fields::INPUT_PARAGRAPH]);
                }
            ])
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->get();

        return view('pages.assessment-criteria.index', ['data' => $data]);
    }

    public function create()
    {
        $users = User::query()
            ->with(['division', 'job'])
            ->select('id', 'name', 'division_id', 'job_id')
//            ->where('id', '<>', auth()->id())
            ->get();
        return view('pages.assessment-criteria.create', [
            'users' => $users,
        ]);
    }

    public function store(Request $request)
    {
        // TODO: add validation
        $assessment = Assessment::query()->create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'status' => AssessmentStatus::PENDING,
        ]);
        $assessment->teams()->createMany(array_map(function ($value) use ($assessment) {
            return ['user_id' => $value];
        }, $request->get('teams')));
        return redirect()->route('kriteria-penilaian-show', ['id' => $assessment->id]);
    }

    public function show($id)
    {
        $assessment = Assessment::query()
            ->with(['teams', 'items'])
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->findOrFail($id);

        $users = User::query()
            ->with(['division', 'job'])
            ->select('id', 'name', 'division_id', 'job_id')
            ->get();
        $selectedTeams = $assessment->teams->pluck('user_id');

        return view('pages.assessment-criteria.edit', [
            'assessment' => $assessment,
            'selectedTeams' => $selectedTeams,
            'users' => $users,
        ]);
    }

    public function edit($id)
    {
        return $assessment = Assessment::query()
            ->with('teams')
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $assessment = Assessment::query()
            ->with(['teams', 'items'])
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->findOrFail($id);

        $teams = $request->get('teams');
        $assessment->teams()->whereNotIn('user_id', $teams)->delete();
        foreach ($teams as $id) {
            $team = $assessment->teams()->where('user_id', $id)->first();
            if (!$team) {
                $assessment->teams()->create(['user_id' => $id]);
            }
        }

        $hasNewItem = false;
        $assessmentItemIds = [];
        foreach ($request->get('items', []) as $key => $item) {
            $itemId = $item['id'] ?? null;
            $data = [
                'title' => $item['title'],
                'description' => $item['description'] ?? null,
                'cost_benefit' => $item['cost_benefit'] ?? null,
                'field_type' => $item['type'],
                'field_value' => json_encode($item['value']) ?? null,
                'group' => $item['group'] ?? null,
                'ordinal_no' => $key + 1,
            ];
            if ($itemId == null) {
                $hasNewItem |= in_array($item['type'], [Fields::INPUT_LINEAR_SCALE, Fields::INPUT_NUMBER]);
                $itemAssessment = $assessment->items()->create($data);
                $assessmentItemIds[] = $itemAssessment->id;
                continue;
            }
            $itemAssessment = $assessment->items()->find($itemId);
            if ($itemAssessment) {
                $itemAssessment->update($data);
                $assessmentItemIds[] = $itemAssessment->id;
            }
        }
        $assessment->items()->whereNotIn('id', $assessmentItemIds)->delete();

        $assessment->title = $request->get('title');
        $assessment->description = $request->get('description');
        if ($hasNewItem) {
            $assessment->teamAssessmentReviews()->delete();
            $assessment->status = AssessmentStatus::REVIEW_BY_TEAMS;
        }
        $assessment->weight = null;
        $assessment->update();

        return [
            'success' => true,
            'message' => 'Berhasil menyimpan',
            'data' => $assessment
        ];
    }

    public function destroy($id)
    {
        //
    }

    public function publish($id)
    {
        $assessment = Assessment::query()
            ->with('teams')
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->find($id);

        if (!$assessment) {
            return redirect()->back();
        }

        if ($assessment->status != AssessmentStatus::MAPPING_USERS) {
            return redirect()
                ->back()
                ->with('error', 'Gagal!');
        }

        $assessment->update([
            'status' => AssessmentStatus::PUBLISHED
        ]);

        return redirect()
            ->back()
            ->with('success', 'Berhasil!')
            ->with('messages', 'Berhasil dipublikasi.');
    }

    public function unpublish($id)
    {
        $assessment = Assessment::query()
            ->with('userAssessmentReviews')
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->find($id);

        if (!$assessment) {
            return redirect()->back();
        }

        if ($assessment->status != AssessmentStatus::PUBLISHED) {
            return redirect()
                ->back()
                ->with('error', 'Gagal!');
        }

        $assessment->userAssessmentReviews()->delete();

        $assessment->update([
            'status' => AssessmentStatus::MAPPING_USERS
        ]);

        return redirect()
            ->back()
            ->with('success', 'Berhasil!')
            ->with('messages', 'Berhasil batalkan publikasi.');
    }
}
