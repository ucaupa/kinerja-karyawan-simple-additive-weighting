<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Models\Assessment;
use App\Models\AssessmentItem;
use App\Models\AssessmentUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class UserAssessmentResponseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AssessmentUser::query()
                ->select('assessment_users.*')
                ->with(['assessment.teams', 'user', 'evaluator', 'reviews.item'])
                ->whereHas('assessment', function ($q) {
                    $q->where(function ($query) {
                        $query->whereHas('teams', function (Builder $query) {
                            $query->where('user_id', auth()->id());
                        });
                        $query->orWhere('created_by', auth()->id());
                    });
                })
                ->when(!empty($request->get('assessment_id')), function ($query) use ($request) {
                    $query->where('assessment_id', $request->get('assessment_id'));
                })
                ->when(!empty($request->get('assessment_id')), function ($query) use ($request) {
                    $query->where('assessment_id', $request->get('assessment_id'));
                })
                ->when(!empty($request->get('evaluator_id')), function ($query) use ($request) {
                    $query->where('evaluator_id', $request->get('evaluator_id'));
                })
                ->when(!empty($request->get('user_id')), function ($query) use ($request) {
                    $query->where('user_id', $request->get('user_id'));
                })
                ->get();

            $columns = AssessmentItem::query()
                ->where('assessment_id', $request->get('assessment_id'))
                ->orderBy('ordinal_no')
                ->assessmentItem()
                ->get()
                ->map(function ($item, $key) {
                    return [
                        'name' => Str::snake($item->title),
                        'title' => $item->title . '(C' . ($key + 1) . ')',
                        'data' => Str::snake($item->title),
                        'orderable' => false,
                        'searchable' => false,
                        'aggregate' => 'avg',
                    ];
                });

            $columns = array_merge(
                [
                    [
                        'name' => 'evaluator',
                        'title' => 'Alternatif',
                        'data' => 'evaluator',
                        'orderable' => false,
                        'searchable' => false,
                    ],
                    [
                        'name' => 'user',
                        'title' => 'Alternatif',
                        'data' => 'user',
                    ],
                ],
                $columns->toArray(),
            );

//            $result = [];
//            foreach ($data as $datum) {
//                if (!array_key_exists($datum->user_id, $result)) {
//                    $result[$datum->user_id] = [
//                        'user' => $datum->user->name,
//                        'data' => [
//                            [
//                                'penilai' => $datum->evaluator->name,
//                                'nilai' => $datum->reviews->pluck('field_value')
//                            ]
//                        ],
//                    ];
//                } else {
//                    $result[$datum->user_id]['data'][] = [
//                        'penilai' => $datum->evaluator->name,
//                        'nilai' => $datum->reviews->pluck('field_value')
//                    ];
//                }
//            }

            return [
                'columns' => $columns,
                'data' => $data->map(function ($item) {
                    $result = [
                        'user' => $item->user->name,
                        'evaluator' => $item->evaluator->name,
                        'assessment_title' => $item->assessment->title,
                    ];
                    foreach ($item->reviews as $review) {
                        $result[Str::snake($review->item->title)] = (float)$review->field_value;
                    }
                    return $result;
                }),
            ];
        }
        $assessments = Assessment::query()
            ->select(['id', 'title'])
            ->where('status', '>', AssessmentStatus::REVIEW_BY_TEAMS)
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->get();
        $users = User::query()->get();
        return view('pages.user-assessments.all', ['users' => $users, 'assessments' => $assessments]);
    }
}
