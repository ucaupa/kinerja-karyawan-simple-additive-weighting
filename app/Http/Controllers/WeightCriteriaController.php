<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Constants\Fields;
use App\Helpers\GeneticAlgorithm;
use App\Models\Assessment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class WeightCriteriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Assessment::query()
            ->with([
                'items' => function ($query) {
                    $query->whereNotIn('field_type', [Fields::TITLE, Fields::INPUT_PARAGRAPH]);
                }
            ])
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->where('status', '>=', AssessmentStatus::MAPPING_USERS)
            ->orderByDesc('updated_at')
            ->get();

        return view('pages.weight-criteria.index', ['data' => $data]);
    }

    public function getWeight($id, Request $request)
    {
        $assessment = Assessment::query()
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->where('status', '>=', AssessmentStatus::MAPPING_USERS)
            ->find($id);

        if (empty($assessment)) {
            if ($request->ajax()) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data tidak Ditemukan'
                ])->setStatusCode(404);
            }
            return redirect()
                ->back()
                ->with('error', 'Data tidak Ditemukan');
        }

        $dataObjectiveTeam = [];
        $resDataObjectiveTeam = [];
        $categories = [];
        $costBenefit = [];
        foreach ($assessment->teams as $index => $team) {
            $user = $team->user;
            if (!isset($dataObjectiveTeam[$team->user_id])) {
                $dataObjectiveTeam[$team->user_id] = [];
                $resDataObjectiveTeam[$user->name] = [
                    'alternative' => $user->name,
                    'code' => 'A' . ($index + 1),
                    'value' => []
                ];
            }
            foreach ($team->reviews as $key => $review) {
                $categories[$key] = 'C' . ($key + 1);
                $costBenefit[$key] = 1;
                $dataObjectiveTeam[$team->user_id][] = $review->field_value;
                $resDataObjectiveTeam[$user->name]['value'][] = $review->field_value;
            }
        }

        $w = new GeneticAlgorithm($dataObjectiveTeam, $categories, $costBenefit, GeneticAlgorithm::FITNESS_SUBJECTIVE_OBJECTIVE);
        $weight = $w->weight();

        $chromosome = $w->getDataBestChromosome();
        $gens = [];
        foreach (array_values($categories) as $key => $value) {
            $val = array_slice($chromosome, $key * 7, 7);
            $gens[] = [
                'slice' => implode(' ', $val),
                'value' => bindec(implode('', $val)),
                'xi' => ((1 / (2 ** 7 - 1)) * bindec(implode('', $val))),
            ];
        }

        $assessment->update([
            'weight' => json_encode($weight)
        ]);

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'data' => $weight,
                'sum' => array_sum($weight)
            ]);
        }

        return redirect()
            ->back()
            ->with('success', 'Berhasil!')
            ->with('messages', 'Bobot Berhasil diupdate.')
            ->with('title', $assessment->title)
            ->with('dataObjective', array_values($resDataObjectiveTeam))
            ->with('dataObjectiveNormalized', $w->getDataObjectiveNormalized())
            ->with('dataPairwiseComparison', $w->getDataPairwiseComparison())
            ->with('chromosome', json_encode($chromosome))
            ->with('gens', $gens)
            ->with('dataWeight', $weight);
    }
}
