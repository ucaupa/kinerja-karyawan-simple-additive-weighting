<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Constants\Fields;
use App\Models\Assessment;
use App\Models\AssessmentUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MappingAlternativeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AssessmentUser::query()
                ->select('assessment_users.*')
                ->with(['assessment.teams', 'user', 'evaluator'])
                ->whereHas('assessment', function ($query) {
                    $query->whereHas('teams', function (Builder $query) {
                        $query->where('user_id', auth()->id());
                    });
                    $query->orWhere('created_by', auth()->id());
                });

            return DataTables::eloquent($data)
                ->addColumn('name', function ($row) {
                    if ($row->user) {
                        return $row->user->name;
                    }
                    return 'Unknown';
                })
                ->addColumn('email', function ($row) {
                    if ($row->user) {
                        return $row->user->email;
                    }
                    return 'Unknown';
                })
                ->addColumn('evaluator_name', function ($row) {
                    if ($row->evaluator) {
                        return $row->evaluator->name;
                    }
                    return 'Unknown';
                })
                ->addColumn('evaluator_email', function ($row) {
                    if ($row->evaluator) {
                        return $row->evaluator->email;
                    }
                    return 'Unknown';
                })
                ->addColumn('action', function ($row) {
                    return '<button type="button" class="btn btn-danger btn-xs btn-delete" data-id="' . $row->id . '">x</button>';
                })
                ->rawColumns(['action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('assessment_id'))) {
                        $instance->where('assessment_id', $request->get('assessment_id'));
                    }
                    if (!empty($request->get('evaluator_id'))) {
                        $instance->where('evaluator_id', $request->get('evaluator_id'));
                    }
                    if (!empty($request->get('user_id'))) {
                        $instance->where('user_id', $request->get('user_id'));
                    }
                })
                ->toJson();
        }
        $assessments = Assessment::query()
            ->select(['id', 'title'])
            ->where('status', '>', AssessmentStatus::REVIEW_BY_TEAMS)
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->get();
        $users = User::query()->get();
        return view('pages.mapping-alternative.index', ['users' => $users, 'assessments' => $assessments]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // TODO: add validations
        $find = AssessmentUser::query()
            ->where('assessment_id', $request->get('assessment_id'))
            ->where('evaluator_id', $request->get('evaluator_id'))
            ->where('user_id', $request->get('user_id'))
            ->first();
        if ($find) {
            return response()->json([
                'success' => false,
                'message' => 'Data sudah ada',
            ]);
        }
        AssessmentUser::query()->create([
            'assessment_id' => $request->get('assessment_id'),
            'evaluator_id' => $request->get('evaluator_id'),
            'user_id' => $request->get('user_id'),
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Data berhasil ditambahkan',
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $data = AssessmentUser::query()
            ->with(['assessment.teams'])
            ->whereHas('assessment', function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->find($id);

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Gagal menghapus alternatif',
            ]);
        }

        $data->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data berhasil dihapus',
        ]);
    }
}
