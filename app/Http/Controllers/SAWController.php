<?php

namespace App\Http\Controllers;

use App\Helpers\GeneticAlgorithm;
use App\Models\Assessment;
use App\Models\AssessmentItem;
use Illuminate\Database\Eloquent\Builder;

class SAWController extends Controller
{
    public function index()
    {
        $id = 1;
        $assessment = Assessment::query()
            ->with([
                'items',
                'teams.reviews.item',
                'teamAssessmentReviews'
            ])
            ->findOrFail($id);

        $dataObjectiveTeam = [];
        $categories = [];
        $costBenefit = [];
        foreach ($assessment->teams as $team) {
            if (!isset($dataObjectiveTeam[$team->user_id])) {
                $dataObjectiveTeam[$team->user_id] = [];
            }
            foreach ($team->reviews as $key => $review) {
                $categories[$key] = 'C' . ($key + 1);
                $costBenefit[$key] = $review->item->cost_benefit ? 1 : 0;
                $dataObjectiveTeam[$team->user_id][] = $review->field_value;
            }
        }

        $dataObjectiveTeam = [
            [7, 7, 7, 7, 7, 5],
            [6, 7, 5, 6, 6.5, 5],
            [7, 7, 6, 7, 6.5, 5.75],
        ];
        $categories = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6'];
        $costBenefit = [1, 1, 1, 1, 1, 1];

//        $w = new GeneticAlgorithm($dataObjectiveTeam, $categories, $costBenefit, GeneticAlgorithm::FITNESS_SUBJECTIVE_OBJECTIVE);
        $w = new GeneticAlgorithm($dataObjectiveTeam, $categories, $costBenefit, GeneticAlgorithm::FITNESS_SUBJECTIVE_OBJECTIVE);
        $weight = $w->weight();

        return response()->json(['data' => $weight, 'sum' => array_sum($weight)]);
    }
}
