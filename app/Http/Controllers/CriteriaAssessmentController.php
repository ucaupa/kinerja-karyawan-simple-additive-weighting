<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Constants\Fields;
use App\Models\Assessment;
use App\Models\AssessmentTeam;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CriteriaAssessmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $fieldValue = [
            ['value' => 1, 'label' => 'Sangat Tidak Setuju'],
            ['value' => 7, 'label' => 'Sangat Setuju']
        ];
        $assessment = Assessment::query()
            ->with(['items' => function ($query) {
                $query->assessmentItemTeam();
            }, 'teams.user'])
            ->whereHas('teams', function (Builder $query) {
                $query->where('user_id', auth()->id());
            })
            ->findOrFail($id);

        foreach ($assessment->items as $item) {
            if ($item->field_type != Fields::TITLE) {
                $item->description = 'Seberapa setuju atau tidak setujukah anda dengan pernyataan diatas dalam penilaian ini?';
                $item->field_type = Fields::INPUT_LINEAR_SCALE;
                $item->field_value = json_encode($fieldValue);
            }
        }

        $copyItem = $assessment->items()->first();
        $copyItem->title = 'Informasi Tambahan';
        $copyItem->description = "Anda diminta memberi rating untuk setiap aspek yang akan menjadi penilaian 360 Feedback\n (1-7, 1 = sangat tidak setuju, 2 = tidak setuju, 3 = agak tidak setuju, 4 = netral, 5 = agak setuju, 6 = setuju, 7 = sangat setuju)\n\n*informasi ini dibuat oleh sistem";
        $copyItem->field_type = Fields::TITLE;
        $assessment->items->prepend($copyItem);

        return view('pages.criteria-assessment.review', ['assessment' => $assessment]);
    }

    public function edit($id)
    {
        $assessment = Assessment::query()
            ->with(['items' => function ($query) {
                $query->assessmentItemTeam();
            }, 'teams.user'])
            ->where(function ($query) {
                $query
                    ->whereHas('teams', function (Builder $query) {
                        $query->where('user_id', auth()->id());
                    })
                    ->orWhere('created_by', auth()->id());
            })
            ->findOrFail($id);

        return view('pages.assessment-criteria.preview', ['assessment' => $assessment]);
    }

    public function update(Request $request, $id)
    {
        $ownAssessment = AssessmentTeam::query()
            ->with(['reviews'])
            ->where('user_id', auth()->id())
            ->where('assessment_id', $id)
            ->firstOrFail();

        // TODO: validation

        $fields = [];
        $validations = [];
        foreach ($request->all() as $key => $value) {
            if (substr($key, 0, 6) === 'input_') {
                $assessmentItemId = (int)substr($key, 6);
                $fields[] = [
                    'assessment_team_id' => $ownAssessment->id,
                    'assessment_item_id' => $assessmentItemId,
                    'field_value' => $value,
                    'created_at' => now()
                ];
            }
        }
        $ownAssessment->reviews()->delete();
        $ownAssessment->reviews()->createMany($fields);

        $assessment = Assessment::query()
            ->withCount([
                'teamAssessmentReviews',
                'teams',
                'items' => function ($query) {
                    $query->assessmentItem();
                }
            ])
            ->findOrFail($id);

        // if all team has done to review then update status to mapping alternative
        $totalItemReview = ($assessment->items_count * $assessment->teams_count);
        $actualReviewCount = $assessment->team_assessment_reviews_count;
        if ($actualReviewCount >= $totalItemReview) {
            $assessment->update([
                'status' => AssessmentStatus::MAPPING_USERS
            ]);
        } else {
            $assessment->update([
                'status' => AssessmentStatus::REVIEW_BY_TEAMS
            ]);
        }

        return redirect()->route('kriteria-penilaian');
    }

    public function destroy($id)
    {
        //
    }
}
