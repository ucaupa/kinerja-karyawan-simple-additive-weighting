<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Models\Assessment;
use App\Models\AssessmentUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserAssessmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AssessmentUser::query()
                ->select('assessment_users.*')
                ->with(['assessment.teams', 'user', 'evaluator', 'reviews.item'])
                ->where('user_id', auth()->id());

            return DataTables::eloquent($data)
                ->addColumn('name', function ($row) {
                    if ($row->user) {
                        return $row->user->name;
                    }
                    return 'Unknown';
                })
                ->addColumn('evaluator_name', function ($row) {
                    if ($row->user) {
                        return $row->evaluator->name;
                    }
                    return 'Unknown';
                })
                ->addColumn('email', function ($row) {
                    if ($row->user) {
                        return $row->user->email;
                    }
                    return 'Unknown';
                })
                ->addColumn('action', function ($row) {
                    return '<button type="button" class="btn btn-info btn-xs btn-detail" data-id="' . $row->id . '">Detail</button>';
                })
                ->rawColumns(['action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('assessment_id'))) {
                        $instance->where('assessment_id', $request->get('assessment_id'));
                    }
                    if (!empty($request->get('evaluator_id'))) {
                        $instance->where('evaluator_id', $request->get('evaluator_id'));
                    }
                    if (!empty($request->get('user_id'))) {
                        $instance->where('user_id', $request->get('user_id'));
                    }
                })
                ->toJson();
        }
        $assessments = Assessment::query()
            ->select(['id', 'title'])
            ->where('status', '>', AssessmentStatus::REVIEW_BY_TEAMS)
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->get();
        $users = User::query()->get();
        return view('pages.user-assessments.index', ['users' => $users, 'assessments' => $assessments]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($assessmentId, $userId)
    {
        $assessmentUser = AssessmentUser::query()
            ->with(['user', 'assessment.items'])
            ->withCount('reviews')
            ->where('assessment_id', $assessmentId)
            ->where('user_id', $userId)
            ->where('evaluator_id', auth()->id())
            ->first();
        if (!$assessmentUser) {
            return redirect()->back()->with('info', 'Tidak Ditemukan');
        }
        if ($assessmentUser->reviews_count > 0) {
            return redirect()->back()->with('info', 'Sudah pernah melakukan penilaian');
        }
        $assessment = $assessmentUser->assessment;
        if ($assessment->status != AssessmentStatus::PUBLISHED) {
            return redirect()->back()->with('info', 'Tidak Ditemukan');
        }

        return view('pages.user-assessments.review', [
            'assessmentUser' => $assessmentUser,
            'userAssessment' => $assessmentUser->user,
            'assessment' => $assessment
        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $assessmentId, $userId)
    {
        $assessmentUser = AssessmentUser::query()
            ->with(['user', 'assessment.items'])
            ->withCount('reviews')
            ->where('assessment_id', $assessmentId)
            ->where('user_id', $userId)
            ->where('evaluator_id', auth()->id())
            ->firstOrFail();

        if ($assessmentUser->reviews_count > 0) {
            return redirect()->route('home')->with('info', 'Sudah pernah melakukan penilaian');
        }

        // TODO: validation

        $fields = [];
        $validations = [];
        foreach ($request->all() as $key => $value) {
            if (substr($key, 0, 6) === 'input_') {
                $assessmentItemId = (int)substr($key, 6);
                $fields[] = [
                    'assessment_user_id' => auth()->id(),
                    'assessment_item_id' => $assessmentItemId,
                    'field_value' => $value,
                    'created_at' => now()
                ];
            }
        }
        $assessmentUser->reviews()->createMany($fields);

        return redirect()
            ->route('penilaian-alternatif-index')
            ->with('success', 'Terimakasih')
            ->with('messages', 'Penilaian terhadap ' . $assessmentUser->user->name . ' telah berhasil.');
    }

    public function destroy($id)
    {
        //
    }
}
