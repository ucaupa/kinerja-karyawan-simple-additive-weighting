<?php

namespace App\Http\Controllers;

use App\Constants\AssessmentStatus;
use App\Helpers\SimpleAdditiveWeighting;
use App\Models\Assessment;
use App\Models\AssessmentUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AssessmentResultController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $id = $request->get('id');
        $assessmentOptions = Assessment::query()
            ->select(['id', 'title', 'weight'])
            ->with([
                'items' => function ($query) {
                    $query->assessmentItem();
                }
            ])
            ->where('status', '>', AssessmentStatus::REVIEW_BY_TEAMS)
            ->where(function ($query) {
                $query->whereHas('teams', function (Builder $query) {
                    $query->where('user_id', auth()->id());
                });
                $query->orWhere('created_by', auth()->id());
            })
            ->orderByDesc('updated_at')
            ->get();

        if ($id && !$assessmentOptions->some('id', $id)) {
            abort(404);
        }

        if (!$id) {
            return view('pages.saw-result.index', [
                'assessments' => $assessmentOptions
            ]);
        }

        $assessment = $assessmentOptions->where('id', $id)->first();
        $assessmentWeight = json_decode($assessment->weight, true);
        if (empty($assessmentWeight) || (count($assessment->items) != count($assessmentWeight))) {
            return redirect()
                ->back()
                ->with('info', 'Info!')
                ->with('messages', 'Bobot pada ' . $assessment->title . ' tidak update, silahkan melakukan generate bobot kembali.');
        }
        $weight = [];
        $costBenefit = [];
        foreach ($assessment->items as $key => $item) {
            $weight[$item->id] = [
                'value' => $assessmentWeight[$key],
                'criteria' => $item->title,
                'type' => $item->cost_benefit == 1 ? 'Benefit' : 'Cost',
            ];
            $costBenefit[] = $item->cost_benefit;
        }
        if (empty($assessmentWeight)) {
            return redirect()
                ->back()
                ->with('info', 'Info!')
                ->with('messages', 'Bobot pada ' . $assessment->title . ' belum dibuat, silahkan generate bobot terlebih dahulu.');
        }

        $assessmentResult = AssessmentUser::query()
            ->with(['user', 'evaluator', 'reviews'])
            ->whereHas('reviews')
            ->where('assessment_id', $id)
            ->get();

        $newWeight = [];
        $res = [];
        foreach ($assessmentResult as $index => $item) {
            $values = $item->reviews->pluck('field_value');

            if ($index == 0) {
                $itemIds = $item->reviews->pluck('assessment_item_id');
                foreach ($itemIds as $itemId) {
                    if (array_key_exists($itemId, $weight)) {
                        $newWeight[] = $weight[$itemId]['value'];
                    }
                }
            }
            if (!array_key_exists($item->user_id, $res)) {
                $res[$item->user_id] = [
                    'count' => 1,
                    'values' => $values,
                    'averages' => [],
                    'user' => $item->user,
                ];
            } else {
                $res[$item->user_id]['count'] += 1;
                foreach ($values as $key => $value) {
                    $res[$item->user_id]['values'][$key] += $value;
                }
            }
            foreach ($values as $key => $value) {
                $res[$item->user_id]['averages'][$key] = $res[$item->user_id]['values'][$key] / $res[$item->user_id]['count'];
            }
        }

        $dataArr = array_values($res);
        $average = Arr::pluck($dataArr, 'averages');
        if (count($average) == 0) {
            return redirect()
                ->back()
                ->with('info', 'Info!')
                ->with('messages', 'Belum ada penilaian pada penilaian: ' . $assessment->title . '.');
        }
        $saw = new SimpleAdditiveWeighting($average, $newWeight, $costBenefit);
        $rank = [];
        foreach ($saw->getPreferenceValue() as $key => $value) {
            $rank[] = [
                'alternative' => $dataArr[$key]['user']->name,
                'code' => 'A' . ($key + 1),
                'value' => $value,
            ];
        }
        $dataAlternatif = [];
        $normalizedData = [];
        foreach ($saw->getNormalized() as $key => $value) {
            $dataAlternatif[] = [
                'alternative' => $dataArr[$key]['user']->name,
                'code' => 'A' . ($key + 1),
                'value' => $average[$key]
            ];
            $normalizedData[] = [
                'alternative' => $dataArr[$key]['user']->name,
                'code' => 'A' . ($key + 1),
                'value' => $value
            ];
        }

        $sortedRank = collect($rank)->sortByDesc('value')->values()->all();

        return view('pages.saw-result.index', [
            'assessments' => $assessmentOptions,
            'weight' => array_values($weight),
            'rank' => $sortedRank,
            'normalizedData' => $normalizedData,
            'dataAlternatif' => $dataAlternatif,
        ]);
    }
}
