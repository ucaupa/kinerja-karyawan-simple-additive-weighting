<?php

namespace App\Http\Middleware;

use App\Constants\AssessmentStatus;
use App\Models\Assessment;
use Closure;
use Illuminate\Http\Request;

class GlobalViewVariable
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $userAssessmentList = Assessment::query()
                ->with([
                    'userAssessments' => function ($query) {
                        $query
                            ->where('evaluator_id', auth()->id())
                            ->whereDoesntHave('reviews');
                    },
                    'userAssessments.user'
                ])
                ->withCount([
                    'userAssessments' => function ($query) {
                        $query
                            ->where('evaluator_id', auth()->id())
                            ->whereDoesntHave('reviews');
                    },
                ])
                ->whereHas('userAssessments', function ($query) {
                    $query
                        ->where('evaluator_id', auth()->id())
                        ->whereDoesntHave('reviews');
                })
                ->where('status', AssessmentStatus::PUBLISHED)
                ->get();
            $teamAssessmentList = Assessment::query()
                ->with([
                    'teamAssessments' => function ($query) {
                        $query
                            ->where('user_id', auth()->id())
                            ->whereDoesntHave('reviews');
                    },
                ])
                ->withCount(['teamAssessments' => function ($query) {
                    $query
                        ->where('user_id', auth()->id())
                        ->whereDoesntHave('reviews');
                }])
                ->whereHas('teamAssessments', function ($query) {
                    $query
                        ->where('user_id', auth()->id())
                        ->whereDoesntHave('reviews');
                })
                ->where('status', AssessmentStatus::REVIEW_BY_TEAMS)
                ->get();
            $totalUserAssessmentNotification = $userAssessmentList->sum('user_assessments_count');
            $totalTeamAssessmentNotification = $teamAssessmentList->sum('team_assessments_count');
            $totalAssessmentNotification = $totalUserAssessmentNotification + $totalTeamAssessmentNotification;

            view()->share('userAssessmentList', $userAssessmentList);
            view()->share('teamAssessmentList', $teamAssessmentList);
            view()->share('totalAssessmentNotification', $totalAssessmentNotification);
        }
        return $next($request);
    }
}
