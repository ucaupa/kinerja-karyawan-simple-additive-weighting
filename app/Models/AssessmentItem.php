<?php

namespace App\Models;

use App\Constants\Fields;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentItem extends Model
{
    use HasFactory;

    protected $table = 'assessment_items';

    protected $fillable = [
        'assessment_id',
        'title',
        'description',
        'cost_benefit',
        'field_type',
        'field_value',
        'group',
        'ordinal_no',
        'created_by',
        'updated_by'
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->created_by = auth()->id();
            $model->updated_by = auth()->id();
        });

        static::updating(function ($model) {
            if ($model->isDirty()) {
                $model->updated_by = auth()->id();
            }
        });
    }

    public function scopeAssessmentItem(Builder $query): void
    {
        $query->whereNotIn('field_type', [Fields::TITLE, Fields::INPUT_PARAGRAPH]);
    }

    public function scopeAssessmentItemTeam(Builder $query): void
    {
        $query->where('field_type', '<>', Fields::INPUT_PARAGRAPH);
    }
}
