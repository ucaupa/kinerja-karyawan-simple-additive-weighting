<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAssessmentResult extends Model
{
    use HasFactory;

    protected $table = 'user_assessment_results';

    protected $fillable = [
        'assessment_user_id',
        'assessment_item_id',
        'field_value'
    ];

    public function item()
    {
        return $this->hasOne(AssessmentItem::class, 'id', 'assessment_item_id');
    }
}
