<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentTeam extends Model
{
    use HasFactory;

    protected $table = 'assessment_teams';

    protected $fillable = [
        'assessment_id',
        'user_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function assessment()
    {
        return $this->hasOne(Assessment::class, 'id', 'assessment_id');
    }

    public function reviews()
    {
        return $this->hasMany(TeamAssessmentResult::class, 'assessment_team_id', 'id');
    }

    public function items()
    {
        return $this
            ->hasMany(AssessmentItem::class, 'assessment_id', 'id')
            ->orderBy('ordinal_no');
    }
}
