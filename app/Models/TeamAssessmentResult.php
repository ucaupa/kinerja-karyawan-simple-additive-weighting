<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamAssessmentResult extends Model
{
    use HasFactory;

    protected $table = 'team_assessment_results';

    protected $fillable = [
        'assessment_team_id',
        'assessment_item_id',
        'field_value'
    ];

    public function item()
    {
        return $this->hasOne(AssessmentItem::class, 'id', 'assessment_item_id');
    }
}
