<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    use HasFactory;

    protected $table = 'assessments';

    protected $fillable = [
        'title',
        'description',
        'status',
        'created_by',
        'updated_by',
        'weight',
    ];

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->created_by = auth()->id();
            $model->updated_by = auth()->id();
        });

        static::updating(function ($model) {
            if ($model->isDirty()) {
                $model->updated_by = auth()->id();
            }
        });
    }

    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function updateBy()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }

    public function teams()
    {
        return $this->hasMany(AssessmentTeam::class, 'assessment_id', 'id');
    }

    public function items()
    {
        return $this
            ->hasMany(AssessmentItem::class, 'assessment_id', 'id')
            ->orderBy('ordinal_no');
    }

    public function teamAssessments()
    {
        return $this->hasMany(AssessmentTeam::class, 'assessment_id', 'id');
    }

    public function userAssessments()
    {
        return $this->hasMany(AssessmentUser::class, 'assessment_id', 'id');
    }

    public function teamAssessmentReviews()
    {
        return $this
            ->hasManyThrough(TeamAssessmentResult::class, AssessmentTeam::class, 'assessment_id', 'assessment_team_id', 'id');
    }

    public function userAssessmentReviews()
    {
        return $this
            ->hasManyThrough(UserAssessmentResult::class, AssessmentUser::class, 'assessment_id', 'assessment_user_id', 'id');
    }
}
