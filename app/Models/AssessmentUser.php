<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentUser extends Model
{
    use HasFactory;

    protected $table = 'assessment_users';

    protected $fillable = [
        'assessment_id',
        'evaluator_id',
        'user_id'
    ];

    public function assessment()
    {
        return $this->hasOne(Assessment::class, 'id', 'assessment_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function evaluator()
    {
        return $this->hasOne(User::class, 'id', 'evaluator_id');
    }

    public function reviews()
    {
        return $this->hasMany(UserAssessmentResult::class, 'assessment_user_id', 'id');
    }
}
