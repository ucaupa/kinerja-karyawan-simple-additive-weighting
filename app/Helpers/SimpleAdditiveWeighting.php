<?php

namespace App\Helpers;

class SimpleAdditiveWeighting
{
    private $alternative;
    private $weight;
    private $costBenefit;
    private $normalizedData;
    private $preferenceValue;

    public function __construct($alternative, $weight, $costBenefit)
    {
        $this->alternative = $alternative;
        $this->weight = $weight;
        $this->costBenefit = $costBenefit;

        $this->normalizedData = $this->normalization();
        $this->preferenceValue = $this->preferenceValue($this->normalizedData);
    }

    public function getNormalized(): array
    {
        return $this->normalizedData;
    }

    public function getPreferenceValue(): array
    {
        return $this->preferenceValue;
    }

    private function preferenceValue($normalizedData): array
    {
        $result = array();
        foreach ($normalizedData as $j => $value) {
            $total = 0;
            foreach ($value as $i => $n) {
                $total += $n * $this->weight[$i];
            }
            $result[$j] = $total;
        }
        return $result;
    }

    private function normalization(): array
    {
        $result = array();
        foreach ($this->alternative as $i => $item) {
            foreach ($item as $j => $value) {
                $min = min(array_column($this->alternative, $j));
                $max = max(array_column($this->alternative, $j));

                if ($this->costBenefit[$j] == 1) {
                    $result[$i][$j] = $value / $max;
                } else {
                    $result[$i][$j] = $min / $value;
                }
            }
        }
        return $result;
    }
}
