<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class GeneticAlgorithm
{
    public $alfa;
    public $beta;
    public $num_var;
    public $num_alternative;
    public $D = [];
    public $B = [];
    public $bestChromosome = [];
    protected $fitnessType;
    public const FITNESS_SUBJECTIVE = 'fitness_sbj';
    public const FITNESS_OBJECTIVE = 'fitness_obj';
    public const FITNESS_SUBJECTIVE_OBJECTIVE = 'fitness_sbj_obj';

    /**
     * @param $dataObjective []
     * @param $categories []
     * @param $costBenefit [0 cost; 1 benefit]
     * @param $type [fitness_sbj, fitness_obj, fitness_sbj_obj]
     */
    public function __construct($dataObjective, $categories, $costBenefit, $type)
    {
        $this->fitnessType = $type;
        $dataObjective = array_values($dataObjective);
//        $dataObjective = [
//            [7, 7, 7, 7, 7, 7, 5, 5, 6, 4],
//            [6, 7, 5, 6, 7, 6, 6, 6, 6, 2],
//            [7, 7, 6, 7, 6, 7, 7, 5, 5, 6],
//        ];

//        $categories = ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10"];

        // 0 cost; 1 benefit
//        $costBenefit = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        $data_objective_normalization = $this->normalizationMatrix($dataObjective, $costBenefit);
        $data_objective_normal = $data_objective_normalization["data"];
        $data_objective_total_column = array_map(function ($val) use ($dataObjective) {
            return $val / count($dataObjective);
        }, $data_objective_normalization["total"]);
        $comparisons = $this->createComparisonMatrix($data_objective_total_column);

        // Matrix perbandingan berpasangan
        $pairwise_comparison = $this->createPairwiseMatrix($categories, $comparisons);

        $this->D = $pairwise_comparison;
        $this->B = $data_objective_normal;
        $this->num_var = count($data_objective_total_column);
        $this->num_alternative = count($data_objective_normal);
        $this->alfa = 0.5;
        $this->beta = 0.5;
    }

    public function weight(): array
    {
        // Parameter-parameter GA
        $bounds = array_fill(0, $this->num_var, [0, 1]);
        $precision = 10 ** 2;
        $population_size = 50; // 50 - ukuran populasi
        $pc = 0.5; // probabilitas crossover
        $pm = 0.01; // probabilitas mutation
        $max_gen = 100; // 100
        $kb = 0.2;

//        dd($this->D);
//        Storage::disk('log-bobot')->put(time() . '.txt', json_encode($this->D, false));
        $weighting_subjective = $this->fsga_madm($bounds, $precision, $population_size, $pc, $pm, $max_gen, $kb, $this->fitnessType);
        if (array_sum($weighting_subjective[0]) != 1) {
            return $this->weight();
        }

        return $weighting_subjective[0];
    }

    public function getDataObjectiveNormalized()
    {
        return $this->B;
    }

    public function getDataPairwiseComparison()
    {
        return $this->D;
    }

    public function getDataBestChromosome()
    {
        return $this->bestChromosome;
    }

    private function createComparisonMatrix($data_total): array
    {
        $count_column = count($data_total);
        $result = [];
        foreach ($data_total as $key => $item) {
            if ($key < $count_column) {
                for ($i = $key + 1; $i < $count_column; $i++) {
                    $item_1_greater_than_item_2 = ($item > $data_total[$i]);
                    if ($item == $data_total[$i]) {
                        $value = 1;
                        $item_1_greater_than_item_2 = true;
                    } else if ($item_1_greater_than_item_2) {
//                        $value = ($item - $data_total[$i]) / max($data_total[$i], $item) * 10;
                        $value = ($item - $data_total[$i]) * 9 + 1;
                    } else {
//                        $value = ($data_total[$i] - $item) / max($data_total[$i], $item) * 10;
                        $value = ($data_total[$i] - $item) * 9 + 1;
                    }
                    if ($value > 9) {
                        $value = 9;
                    }
                    $result[] = [
                        "item1" => $key,
                        "item2" => $i,
                        "value" => ceil($value),
                        "is_greater_than" => $item_1_greater_than_item_2
                    ];
                }
            }
        }
        return $result;
    }

    private function createPairwiseMatrix($items, $comparisons): array
    {
        $matrix = [];
        $numItems = count($items);

        for ($i = 0; $i < $numItems; $i++) {
            $matrix[$i] = array_fill(0, $numItems, 1);
        }

        foreach ($comparisons as $comparison) {
            $item1 = $comparison['item1'];
            $item2 = $comparison['item2'];
            $value = $comparison['value'];
            $is_greater_than = (boolean)$comparison['is_greater_than'];

            if ($value == 0) dd($comparison); // FIXME: why?
            if ($is_greater_than) {
                $matrix[$item1][$item2] = $value;
                $matrix[$item2][$item1] = 1 / $value;
            } else {
                $matrix[$item1][$item2] = 1 / $value;
                $matrix[$item2][$item1] = $value;
            }
        }

        return $matrix;
    }

    private function normalizationMatrix($data, $cost_benefit): array
    {
        $transpose = $this->transpose($data);
        $result = [];
        $sum_result = [];
        $min_all = min(array_map('min', $data));
        $max_all = max(array_map('max', $data));
        foreach ($transpose as $i => $val) {
            $min = min($val);
            $max = max($val);
            foreach ($val as $j => $value) {
                $val_sum = 0;
                // normalisasi min/max
                if ($cost_benefit[$i] == 1) { // (A($k,$i)-MinA($i))/(MaxA($i)-MinA($i))
                    $pembilang = ($value - $min);
                    $val_sum = $value / $max_all;
                } else { // (MaxA($i)-A($k,$i))/(MaxA($i)-MinA($i))
                    $pembilang = ($max - $value);
                    $val_sum = $min_all / $value;
                }
                $penyebut = ($max - $min);
                if ($pembilang == 0 && $penyebut == 0) $result[$j][$i] = 0;
                else $result[$j][$i] = $pembilang / $penyebut;

                // menjumlahkan nilai pada kolom atribut
                if (!isset($sum_result[$i])) {
                    $sum_result[$i] = $val_sum;
                } else {
                    $sum_result[$i] += $val_sum;
                }
            }
        }
        return [
            "data" => $result,
            "total" => $sum_result,
        ];
    }

    function minMaxNormalization($matrix): array
    {
        $min = min(array_map('min', $matrix));
        $max = max(array_map('max', $matrix));

        $normalizedMatrix = [];

        foreach ($matrix as $row) {
            $normalizedRow = array_map(function ($value) use ($min, $max) {
                return ($value - $min) / ($max - $min);
            }, $row);
            $normalizedMatrix[] = $normalizedRow;
        }

        return $normalizedMatrix;
    }

    private function transpose($data): array
    {
        $result = [];
        foreach ($data as $key => $item) {
            foreach ($item as $sub_key => $sub_item) {
                $result[$sub_key][$key] = $sub_item;
            }
        }
        return $result;
    }

    private function fsga_madm($bounds, $presisi, $population_size, $pc, $pm, $MaxGen, $kb, $fitness_function)
    {
        $bits = [];
        // Menghitung panjang kromosom pada setiap n gen
        for ($i = 0; $i < $this->num_var; $i++) {
            $bits[$i] = $this->calcbits($bounds[$i], $presisi);
        }
        // Panjang kromosom, attribute * panjang n gen
        $lebar = array_sum($bits);
        $population = [];
        for ($i = 0; $i < $population_size; $i++) {
            $population[$i] = [];
            for ($j = 0; $j < $lebar; $j++) {
                $population[$i][$j] = mt_rand(0, 1);
            }
        }

        // Menentukan populasi awal
        for ($i = 0; $i < $population_size; $i++) {
            $n_bit = 0;
            $chromosome = [];
            for ($k = 0; $k < $this->num_var; $k++) {
                $chromosome[$k] = $this->binary_to_float(array_slice($population[$i], $n_bit, $bits[$k]), $bounds[$k], $bits[$k]);
                $n_bit += $bits[$k];
            }
            $w = $this->normalization($chromosome);
            for ($k = 0; $k < $this->num_var; $k++) {
                $population[$i][$lebar + $k] = $w[$k];
            }
            $population[$i][$lebar + $this->num_var] = $this->$fitness_function($w);
            $population[$i][$lebar + $this->num_var + 1] = $i + 1;
        }

        $generasi = 0;
        $VBaik = array_fill(0, $MaxGen, array_fill(0, $this->num_var, 0));
        $baik = array_fill(0, $MaxGen, 0);
        $buruk = array_fill(0, $MaxGen, 0);
        $rata2 = array_fill(0, $MaxGen, 0);

        $parent = [];
        $selek = [];
        // Mulai iterasi sampai maksimum generasi
        while ($generasi < $MaxGen) {
            $generasi++;
            $F = array_sum(array_column($population, $lebar + $this->num_var));

            // Probabilitas fitness
            $p = [];
            $q = [];
            for ($i = 0; $i < $population_size; $i++) {
                $p[$i] = $population[$i][$lebar + $this->num_var] / $F;
                $q[$i] = array_sum(array_slice($p, 0, $i + 1));
            }

            [$kolom_rank_1, $index_1] = $this->sort_array(array_column($population, $lebar + $this->num_var));

            $r = array_map(function () {
                return mt_rand() / mt_getrandmax();
            }, array_fill(0, $population_size, 0));

            // seleksi dengan roda rolet
            for ($i = 0; $i < $population_size; $i++) {
                $temu = 0;
                $j = 0;
                while ($j < ($population_size - 1) && $temu == 0) {
                    if ($r[$i] <= $q[0]) {
                        $selek[$i] = $population[0];
                        $temu = 1;
                    } else if ($r[$i] > $q[$j] && $r[$i] <= $q[$j + 1]) {
                        $selek[$i] = $population[$j + 1];
                        $temu = 1;
                    }
                    $j++;
                }
            }
            for ($i = 0; $i < $population_size; $i++) {
                $selek[$i][$lebar + $this->num_var + 1] = $i + 1;
            }

            $old_population = $population;
            $population = $selek;

            $r = array_map(function () {
                return mt_rand() / mt_getrandmax();
            }, array_fill(0, $population_size, 0));
            $find = [];

            $k = 0;
            // one-point crossover
            for ($i = 0; $i < $population_size; $i++) {
                if ($r[$i] < $pc) {
                    $parent[$k] = $population[$i];
                    $k++;
                }
            }
            if ($k % 2 !== 0) {
                $k--;
            }
            for ($i = 0; $i < $k - 1; $i += 2) {
                $random_position = $this->random_permutation($lebar);
                while ($random_position[0] <= 1 || $random_position[0] >= $lebar) {
                    $random_position = $this->random_permutation($lebar);
                }
                $position = $random_position[0];

                $tmp11 = array_slice($parent[$i], 0, $position);
                $tmp12 = array_slice($parent[$i], $position, $lebar - $position);
                $tmp21 = array_slice($parent[$i + 1], 0, $position);
                $tmp22 = array_slice($parent[$i + 1], $position, $lebar - $position);

                $anak[0] = array_merge($tmp11, $tmp22);
                $anak[1] = array_merge($tmp21, $tmp12);
                $population[$parent[$i][$lebar + $this->num_var + 2 - 1] - 1] = array_replace($population[$parent[$i][$lebar + $this->num_var + 2 - 1] - 1], $anak[0]);
                $population[$parent[$i + 1][$lebar + $this->num_var + 2 - 1] - 1] = array_replace($population[$parent[$i + 1][$lebar + $this->num_var + 2 - 1] - 1], $anak[1]);
            }

            $r = array_map(function () {
                return mt_rand() / mt_getrandmax();
            }, array_fill(0, $population_size * $lebar, 0));

            $l = 0;
            // Kromosom & lokasi kena mutasi
            for ($i = 0; $i < $population_size; $i++) {
                for ($j = 0; $j < $lebar; $j++) {
                    if ($r[$l] < $pm) {
                        if ($population[$i][$j] === 0) {
                            $population[$i][$j] = 1;
                        } else if ($population[$i][$j] === 1) {
                            $population[$i][$j] = 0;
                        }
                    }
                    $l++;
                }
            }

            // Populasi Setelah Crossover & Mutasi:
            for ($i = 0; $i < $population_size; $i++) {
                $n_bit = 0;
                $chromosome = [];
                for ($k = 0; $k < $this->num_var; $k++) {
                    $chromosome[$k] = $this->binary_to_float(array_slice($population[$i], $n_bit, $bits[$k]), $bounds[$k], $bits[$k]);
                    $n_bit += $bits[$k];
                    $population[$i][$lebar + $k] = $chromosome[$k];
                }
                $w = $this->normalization($chromosome);
                for ($k = 0; $k < $this->num_var; $k++) {
                    $population[$i][$lebar + $k] = $w[$k];
                }
                $population[$i][$lebar + $this->num_var] = $this->$fitness_function($w);
                $population[$i][$lebar + $this->num_var + 1] = $i + 1;
            }

            [$kolom_rank_2, $index_2] = $this->sort_array(array_column($population, $lebar + $this->num_var));
            for ($no = 0; $no < round($kb * $population_size); $no++) {
                $population[$index_2[$no]] = $old_population[$index_1[$population_size - $no - 1]];
            }

            // populasi akhir
            for ($i = 0; $i < $population_size; $i++) {
                $n_bit = 0;
                $chromosome = [];
                for ($k = 0; $k < $this->num_var; $k++) {
                    $chromosome[$k] = $this->binary_to_float(array_slice($population[$i], $n_bit, $bits[$k]), $bounds[$k], $bits[$k]);
                    $n_bit += $bits[$k];
                    $population[$i][$lebar + $k] = $chromosome[$k];
                }
                $w = $this->normalization($chromosome);
                for ($k = 0; $k < $this->num_var; $k++) {
                    $population[$i][$lebar + $k] = $w[$k];
                }
                $population[$i][$lebar + $this->num_var] = $this->$fitness_function($w);
                $population[$i][$lebar + $this->num_var + 1] = $i + 1;
            }

            // Update kromosom terbaik, fitness terbaik, terburuk, rata2
            $column_fitness = array_column($population, $lebar + $this->num_var);
            $best_fitness = max($column_fitness);
            [$best_fitness_key] = array_keys($column_fitness, max($column_fitness));
            $baik[$generasi - 1] = $best_fitness;
            for ($k = 0; $k < $this->num_var; $k++) {
                $VBaik[$generasi - 1][$k] = $population[$best_fitness_key][$lebar + $k];
            }
            $buruk[$generasi - 1] = min($column_fitness);
            $rata2[$generasi - 1] = array_sum($column_fitness) / $population_size;
            $this->bestChromosome = array_slice($population[$best_fitness_key], 0, $lebar);
        }
        // Rekap proses sampai maksimum generasi
        $rekap = [];
        for ($j = 0; $j < count($VBaik); $j++) {
            $rekap[] = array_slice($VBaik[$j], 0, $this->num_var);
        }
        // Rekap hasil (Generasi, x, Terbaik, Terburuk, Rata-rata):
        for ($j = 0; $j < count($rekap); $j++) {
            $rekap[$j] = array_merge($rekap[$j], [$baik[$j]], [$buruk[$j]], [$rata2[$j]]);
        }
        $w = $VBaik[$generasi - 1];
        $fs_madm = 1 / $rekap[$generasi - 1][$this->num_var];
        return [$w, $fs_madm];
    }

    /**
     * halaman 106 persamaan (106, 3.46)
     *
     * @param $bound
     * @param $precision
     * @return float
     */
    private function calcbits($bound, $precision): float
    {
        $val = ($bound[1] - $bound[0]) * $precision + 1;
        $log_val = log($val) / log(2);
        return ceil($log_val);
    }

    /**
     * binary to float - (106, 3.47)
     *
     * @param $binary
     * @param $bounds
     * @param $bits
     * @return float
     */
    private function binary_to_float($binary, $bounds, $bits): float
    {
        $a = $bounds[0];
        $b = $bounds[1];
        return $a + (($b - $a) / (2 ** $bits - 1)) * bindec(implode('', $binary));
    }

    private function array_cumulative_sum($bits): array
    {
        if (gettype($bits) == 'array') {
            $cs = [0];
            for ($i = 0; $i < $bits; $i++) {
                $cs[] = $cs[$i] + $bits;
            }
        } else {
            $cs = [$bits];
        }
        return $cs;
    }

    /**
     * return normalized w, untuk memenuhi batasan w = 1
     *
     * @param $krom
     * @return array
     */
    private function normalization($krom): array
    {
        $nw = array_sum($krom);
        $w = [];

        for ($k = 0; $k < $this->num_var; $k++) {
            $w[] = $krom[$k] / $nw;
        }

        return $w;
    }

    /**
     * find fitness value on subjective approach
     *
     * @param $w
     * @return float|int
     */
    private function fitness_sbj($w)
    {
        $evl = 0;
        for ($i = 0; $i < $this->num_var; $i++) {
            for ($j = 0; $j < $this->num_var; $j++) {
                $evl += ($this->D[$i][$j] * $w[$j] - $w[$i]) ** 2;
            }
        }

        return 1 / $evl;
    }

    /**
     * find fitness value on objective approach
     *
     * @param $w
     * @return float|int
     */
    function fitness_obj($w)
    {
        $Bb = max($this->B);

        $evl = 0;
        for ($i = 0; $i < count($this->B); $i++) {
            for ($j = 0; $j < $this->num_var; $j++) {
                $evl += (($Bb[$j] - $this->B[$i][$j]) ** 2) * ($w[$j] ** 2);
            }
        }

        return 1 / $evl;
    }

    /**
     * find fitness value integrated subjective & objective approach
     *
     * @param $w
     * @return float|int
     */
    function fitness_sbj_obj($w)
    {
        $Bb = max($this->B);

        $evl1 = 0;
        for ($i = 0; $i < $this->num_var; $i++) {
            for ($j = 0; $j < $this->num_var; $j++) {
                $evl1 += ($this->D[$i][$j] * $w[$j] - $w[$i]) ** 2;
            }
        }

        $evl2 = 0;
        for ($i = 0; $i < count($this->B); $i++) {
            for ($j = 0; $j < $this->num_alternative; $j++) {
                $evl2 += (($Bb[$j] - $this->B[$i][$j]) ** 2) * ($w[$j] ** 2);
            }
        }

        return 1 / ($this->alfa * $evl1 + $this->beta * $evl2);
    }

    private function sort_array($data): array
    {
        // save the original indices
        $originalIndices = range(0, count($data) - 1);

        // perform manual sorting (bubble sort in this example)
        $n = count($data);
        do {
            $swapped = false;
            for ($i = 0; $i < $n - 1; $i++) {
                if ($data[$i] > $data[$i + 1]) {
                    // swap values
                    $temp = $data[$i];
                    $data[$i] = $data[$i + 1];
                    $data[$i + 1] = $temp;

                    // swap indices
                    $tempIndex = $originalIndices[$i];
                    $originalIndices[$i] = $originalIndices[$i + 1];
                    $originalIndices[$i + 1] = $tempIndex;

                    $swapped = true;
                }
            }
        } while ($swapped);

        $index = $originalIndices;

        return [$data, $index];
    }

    /**
     * random permutation of the integers
     *
     * @param $n
     * @return array
     */
    private function random_permutation($n): array
    {
        $arr = range(1, $n);
        shuffle($arr);
        return $arr;
    }
}
