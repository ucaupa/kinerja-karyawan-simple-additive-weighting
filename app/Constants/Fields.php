<?php

namespace App\Constants;

class Fields
{
    const INPUT_LINEAR_SCALE = 'INPUT_LINEAR_SCALE';
    const INPUT_NUMBER = 'INPUT_NUMBER';
    const INPUT_PARAGRAPH = 'INPUT_PARAGRAPH';
    const TITLE = 'TITLE';

    const LIST_OPTIONS = [
        'INPUT_LINEAR_SCALE' => 'Skala Linier',
//        'INPUT_NUMBER' => 'Input Angka',
//        'INPUT_PARAGRAPH' => 'Paragraf'
    ];
}
