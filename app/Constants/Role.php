<?php

namespace App\Constants;

class Role
{
    const ROLE_GUEST = 0;
    const ROLE_ADMIN = 1;
    const ROLE_MANAGEMENT = 2;
    const ROLE_EMPLOYEE = 3;
}
