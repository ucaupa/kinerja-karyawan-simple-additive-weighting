<?php

namespace App\Constants;

class AssessmentStatus
{
    const PENDING = 0;
    const REVIEW_BY_TEAMS = 1;
    const MAPPING_USERS = 2;
    const PUBLISHED = 3;

    const LIST_OPTIONS = [
        self::PENDING => 'Atur Kriteria Penilaian',
        self::REVIEW_BY_TEAMS => 'Penilaian Kriteria oleh Tim Penilai',
        self::MAPPING_USERS => 'Mapping Alternatif',
        self::PUBLISHED => 'Telah Dipublikasi',
    ];
}
