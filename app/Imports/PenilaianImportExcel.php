<?php

namespace App\Imports;

use App\Models\AssessmentItem;
use App\Models\AssessmentUser;
use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PenilaianImportExcel implements ToCollection
{
    private $assessmentId;

    public function __construct($assessmentId)
    {
        $this->assessmentId = $assessmentId;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $assessmentItems = AssessmentItem::query()
            ->where('assessment_id', $this->assessmentId)
            ->assessmentItem()
            ->orderBy('ordinal_no')
            ->get();
        if (count($assessmentItems) == 0) return;
        $users = [];
        foreach ($collection as $key => $item) {
            if ($key == 0) {
                continue;
            }
            $this->checkUser($users, $item[0]);
            $this->checkUser($users, $item[1]);
            $result = null;
            if ($users[$item[0]] != null && $users[$item[1]] != null) {
                $result = [
                    'reviewer_id' => $users[$item[0]]['id'],
                    'reviewer_name' => $users[$item[0]]['name'],
                    'user_id' => $users[$item[1]]['id'],
                    'user_name' => $users[$item[1]]['name'],
                ];
            }
            if (empty($result)) continue;
            $assessmentUser = AssessmentUser::query()->firstOrCreate([
                'assessment_id' => $this->assessmentId,
                'evaluator_id' => $result['reviewer_id'],
                'user_id' => $result['user_id']
            ]);
            $fields = [];
            foreach ($item as $index => $value) {
                if ($index < 2 || $value == null) continue;
                if (!array_key_exists(($index - 2), $assessmentItems->toArray())) continue;
                $fields[] = [
                    'assessment_user_id' => $assessmentUser->id,
                    'assessment_item_id' => $assessmentItems->toArray()[$index - 2]['id'],
                    'field_value' => $value,
                    'created_at' => now(),
                ];
            }
            $assessmentUser->reviews()->delete();
            $assessmentUser->reviews()->createMany($fields);
        }
    }

    private function checkUser(&$users, $key)
    {
        if (!array_key_exists($key, $users)) {
            $checkUser = User::query()->where('email', $key)->first();
            $users[$key] = empty($checkUser) ? null : $checkUser->toArray();
        }
    }
}
