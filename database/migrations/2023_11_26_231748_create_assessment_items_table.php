<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('assessment_id');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->boolean('cost_benefit')->nullable();
            $table->string('field_type');
            $table->longText('field_value')->nullable();
            $table->string('group')->nullable();
            $table->integer('ordinal_no');
            $table->foreignId('created_by')->constrained('users')->cascadeOnDelete();
            $table->foreignId('updated_by')->nullable()->constrained('users')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_items');
    }
}
