<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResponseToTeamAssessmentResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_assessment_results', function (Blueprint $table) {
            $table->longText('field_value')->after('assessment_item_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_assessment_results', function (Blueprint $table) {
            $table->dropColumn('field_value');
        });
    }
}
