<?php

namespace Database\Seeders;

use App\Models\Division;
use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Accounting & Finance",
            "Bisnis",
            "Engineering",
            "Marketing",
            "Operation",
            "Product",
            "Product & Operation",
            "UI/UX"
        ];

        Division::query()->insert(array_map(function ($value) {
            return [
                'name' => $value,
                'created_at' => now(),
            ];
        }, $data));
    }
}
