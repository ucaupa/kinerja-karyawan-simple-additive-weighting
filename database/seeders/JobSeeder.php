<?php

namespace Database\Seeders;

use App\Models\Job;
use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Account Executive",
            "Accounting Staff",
            "Business Analyst",
            "Business Development (Event)",
            "Business Implementation Lead",
            "Business Implementation Support",
            "Business Intelligence",
            "Business Intelligence Lead ",
            "Business Operation Lead",
            "Business Operation Support",
            "Business Partnership",
            "Chief Executive Officer",
            "Chief Operating Officer",
            "Chief Technology Officer",
            "Corporate Finance Strategist",
            "Customer Service",
            "Digital Marketing",
            "Engineering Manager",
            "Graphic Designer",
            "Head UI/UX",
            "Head of Accounting",
            "Head of Engineering",
            "Head of Mobile",
            "Head of Project Manager",
            "Managing Director",
            "Marketing Strategist",
            "Mobile Engineer",
            "Product Design",
            "Product Manager",
            "Project Manager",
            "Recruitment & HR Associate",
            "Software Engineer",
            "Software Quality Assurance",
            "System Analyst",
            "UX Designer"
        ];

        Job::query()->insert(array_map(function ($value) {
            return [
                'name' => $value,
                'created_at' => now(),
            ];
        }, $data));
    }
}
