<?php

namespace Database\Seeders;

use App\Imports\PenilaianImportExcel;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class PenilaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new PenilaianImportExcel(1), database_path() . '/data/Data Penilaian 360.xlsx');
    }
}
