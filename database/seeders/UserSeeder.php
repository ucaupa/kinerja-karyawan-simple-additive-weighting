<?php

namespace Database\Seeders;

use App\Constants\Role;
use App\Models\Division;
use App\Models\Job;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $managements = [
            "Account Executive",
//            "Accounting Staff",
//            "Business Analyst",
            "Business Development (Event)",
            "Business Implementation Lead",
            "Business Implementation Support",
            "Business Intelligence",
            "Business Intelligence Lead ",
            "Business Operation Lead",
            "Business Operation Support",
            "Business Partnership",
            "Chief Executive Officer",
            "Chief Operating Officer",
            "Chief Technology Officer",
            "Corporate Finance Strategist",
//            "Customer Service",
//            "Digital Marketing",
            "Engineering Manager",
//            "Graphic Designer",
            "Head UI/UX",
            "Head of Accounting",
            "Head of Engineering",
            "Head of Mobile",
            "Head of Project Manager",
            "Managing Director",
            "Marketing Strategist",
//            "Mobile Engineer",
//            "Product Design",
            "Product Manager",
            "Project Manager",
            "Recruitment & HR Associate",
//            "Software Engineer",
//            "Software Quality Assurance",
//            "System Analyst",
//            "UX Designer"
        ];
        $data_users = json_decode(file_get_contents(database_path() . '/data/users.json'), true);
        $users = array_map(function ($item) use ($managements) {
            return [
                'name' => $item['first_name'] . ' ' . $item['last_name'],
                'email' => $item['email'],
                'password' => Hash::make('12341234'),
                'role' => in_array($item['job'], $managements) ? Role::ROLE_MANAGEMENT : Role::ROLE_EMPLOYEE,
                'division_id' => Division::query()->where('name', $item['organization'])->first()->id,
                'job_id' => Job::query()->where('name', $item['job'])->first()->id,
                'created_at' => now()
            ];
        }, $data_users);
        User::query()->insert($users);
    }
}
