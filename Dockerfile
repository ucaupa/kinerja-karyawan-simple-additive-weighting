# Dockerfile
#FROM php:7.4-cli
#
#RUN apt-get update -y && apt-get install -y libmcrypt-dev
#
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#RUN docker-php-ext-install pdo
#
#WORKDIR /app
#COPY . /app
#
#RUN composer install

# Gunakan image resmi PHP dengan Apache
FROM php:7.4-apache

# Set direktori kerja ke direktori proyek Laravel
WORKDIR /app

# Salin seluruh isi proyek ke dalam container
COPY . .

# Atur hak akses ke direktori penyimpanan dan cache Laravel
RUN chown -R www-data:www-data storage bootstrap/cache

# Pasang Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install ekstensi PHP yang diperlukan
RUN docker-php-ext-install pdo pdo_mysql

# Install dependensi PHP menggunakan Composer
RUN composer install --no-interaction

## Atur konfigurasi Apache
#COPY docker/apache2.conf /etc/apache2/sites-available/000-default.conf
#RUN a2enmod rewrite

# Expose port 80 untuk Apache
EXPOSE 7001

# Perintah untuk menjalankan Apache ketika container dimulai
CMD php artisan serve --host=0.0.0.0 --port=7001
